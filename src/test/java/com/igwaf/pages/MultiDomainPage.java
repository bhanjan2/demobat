package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;
import com.igwaf.util.RouteConfig;

public class MultiDomainPage {
	
public static Logger log= Logger.getLogger(MultiDomainPage.class);
	
//-----------------------------------------------------------------------------                       
//								PAGE ELEMENTS
//----------------------------------------------------------------------------- 
	
	@FindBy(xpath="//div[text()='All Sites - Health Summary']")
	public static WebElement healthSummaryLabel;
		
	@FindBy(xpath="(//img[@title='Logout'])[2]")
	public static WebElement logOutButton;
	

	@FindBy(xpath="//button[text()='Add Website']")
	public static WebElement addWebsiteButton;
	
	@FindBy(xpath="//div[text()='Add Website']")
	public static WebElement addWebsiteSection;
	
	@FindBy(xpath="(//input[@class='GNN-SDDCLM'])[1]")
	public static WebElement accountHolderNameTextbox;
	
	@FindBy(xpath="(//input[@class='GNN-SDDCLM'])[2]")
	public static WebElement creditCardNumberTextbox;
	
	@FindBy(xpath="//input[@class='GNN-SDDCKM']")
	public static WebElement cvvNumberTextbox;
	
	@FindBy(xpath="//select[@class='GNN-SDDCJM']")
	public static WebElement monthSelector;
	
	@FindBy(xpath="(//select[@class='GNN-SDDCJM'])[2]")
	public static WebElement yearSelector;
	
	@FindBy(xpath="//button[text()='Next']")
	public static WebElement nextButton;
	
	@FindBy(xpath="//button[text()='Submit']")
	public static WebElement submitButton;
	
//	@FindBy(xpath="(//input[@class='GNN-SDDCMO'])[1]")
	@FindBy(xpath="(//input[@class='GNN-SDDCLO'])[1]")
	public static WebElement domainNameTextbox;
	
//	@FindBy(xpath="(//select[@class='GNN-SDDCKO'])[2]")
	@FindBy(xpath="(//select[@class='GNN-SDDCJO'])[2]")
	public static WebElement deploymentTypeSelector;
	
//	@FindBy(xpath="(//input[@class='GNN-SDDCMO'])[2]")
	@FindBy(xpath="(//input[@class='GNN-SDDCLO'])[2]")
	public static WebElement forwardingIpAddressTextbox;
	
	@FindBy(xpath="(//span[@class='checkbox']/input)[2]")
	public static WebElement leftHttp;
	
	@FindBy(xpath="(//span[@class='checkbox']/input)[4]")
	public static WebElement rightHttp;
	
	@FindBy(xpath="//div[@class='multisites-link']")
	public static WebElement domainName;
	
	@FindBy(xpath="(//span[@class='checkbox']/input)[1]")
	public static WebElement leftHttps;
	
	@FindBy(xpath="(//span[@class='checkbox']/input)[3]")
	public static WebElement rightHttps;
	
	@FindBy(xpath="(//textarea[@class='GNN-SDDCFN'])[1]")
	public static WebElement httpsPrivateKey;
	
	@FindBy(xpath="(//textarea[@class='GNN-SDDCFN'])[2]")
	public static WebElement httpsCertificate;
	
	@FindBy(xpath="(//textarea[@class='GNN-SDDCFN'])[3]")
	public static WebElement httpsChainCertificate;
	
	@FindBy(xpath="//input[@class='GNN-SDDCGN']")
	public static WebElement httpsPrivateKeyPassphrase;
	
	@FindBy(xpath="//button[text()='Skip']")
	public static WebElement skipButton;
	
	@FindBy(xpath="//button[text()='CONTINUE']")
	public static WebElement continueButton;
	
	@FindBy(xpath="//div[text()='Scan Request']")
	public static WebElement scanRequestSection;
	
	@FindBy(xpath="//div[text()='Scan Request']/../..//img")
	public static WebElement scanRequestSectionCloseButton;
	
	@FindBy(xpath="//img[@src='/dashboard/images/profile.png']")
	 public static WebElement profile;
	
	@FindBy(xpath="//table[@class='table']/tbody/tr[2]/td[@class='multisites-col-name-value']/following-sibling::td[1]/table/tbody/tr[2]")
	public static WebElement routeConfigRequiredLabel;
	
	
	@FindBy(xpath="//table[@class='table']/tbody/tr[2]/td[@class='multisites-col-name-value']/following-sibling::td[1]/img[@title='Website is configured to route traffic through Indusface Web Application Firewall']")
	public static WebElement routeConfigDonelabel;  	
	
	@FindBy(xpath="//img[contains(@title,'Routing change required to route website traffic through Indusface Web Application Firewall')]")
	public static WebElement routingChangeRequired;
	
	@FindBy(xpath="//img[contains(@title,'Website is configured to route traffic through Indusface Web Application Firewall')]")
	public static WebElement webSiteConfigured;
	
	@FindBy(xpath="(//td[@class='multisites-col-lastscan-value'])[1]/div")
	public static WebElement lastScanValue;
	
	@FindBy(xpath="//a[contains(text(),'indussecure.com')]")
	public static WebElement webSiteLink;
	
	@FindBy(xpath="//img[@src='/dashboard/images/logo.png']")
	public static WebElement indusFaceLogo;
	
	
//-----------------------------------------------------------------------------                       
//								PAGE METHODS
//----------------------------------------------------------------------------- 
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, MultiDomainPage.class);
	}
	
	
	/**
	 * This is developed by nagendra
	 * logOutFromTAS is used to Logout from the TAS by clicking on logout button
	 * This method return type is void
	 */
	public static void logOutFromTAS()
	{
		try{
		CommonUtil.clickOnButton(logOutButton);
		}
        catch(Exception e)
		{
			CommonUtil.pressEnterThroughActions();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			CommonUtil.pressEnterThroughActions();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			CommonUtil.pressEnterThroughActions();
		}
		
	}
	
	/**
	 * This is developed by nagendra
	 * clickOnAddWebsiteButton is used to click on add website button from 
	 * This method return type is void
	 */
	public static void clickOnAddWebsiteButton()
	{
		CommonUtil.clickOnButton(addWebsiteButton);
	}
	
	/**
	 * This is developed by nagendra
	 * validateAddWebsiteSection is used to validate whether the Add Website section is 
	 * 		displayed or not
	 * This method return type is void
	 */
	public static void validateAddWebsiteSection()
	{
		boolean isAddWebsiteSectionDisplayed = CommonUtil.isElementDisplayed(addWebsiteSection);
		Assert.assertTrue(isAddWebsiteSectionDisplayed,"Add Website Section is not displayed");
    	log.info("Add Website Section is displayed");
		
	}
	
	/**
	 * This is developed by nagendra
	 * enterAccountHolderNameInTextbox is used to enter Account Holder Name in text box
	 * @param name
	 * This method return type is void
	 */
	public static void enterAccountHolderNameInTextbox(String name)
	{
		CommonUtil.enterTextInTextBox(accountHolderNameTextbox, name);
	}
	
	/**
	 * This is developed by nagendra
	 * enterCreditCardNumberInTextbox is used to enter Credit Card Number in text box
	 * @param creditCardNumberValue
	 * This method return type is void
	 */
	public static void enterCreditCardNumberInTextbox(String creditCardNumberValue)
	{
		CommonUtil.enterTextInTextBox(creditCardNumberTextbox, creditCardNumberValue);
	}
	
	/**
	 * This is developed by nagendra
	 * enterCvvNumberInTextbox is used to enter CVV Number in text box
	 * @param cvv
	 * This method return type is void
	 */
	public static void enterCvvNumberInTextbox(String cvv)
	{
		CommonUtil.enterTextInTextBox(cvvNumberTextbox, cvv);
	}
	
	/**
	 * This is developed by nagendra
	 * selectMonthFromDropdown is used to select month from month dropdown
	 * @param option
	 * here option is visble text of the option in month drop down
	 * This method return type is void
	 */
	public static void selectMonthFromDropdown(String option)
	{
		CommonUtil.selectOptionFromDropdownByVisibleText(monthSelector, option);
	}
	
	/**
	 * This is developed by nagendra
	 * selectYearFromDropdown is used to select year from month dropdown
	 * @param option
	 * here option is visble text of the option in year drop down
	 * This method return type is void
	 */
	public static void selectYearFromDropdown(String option)
	{
			CommonUtil.selectOptionFromDropdownByVisibleText(yearSelector, option);
	}
	
	/**
	 * This is developed by nagendra
	 * clickOnNextButton is used to click on Next button
	 * This method return type is void
	 */
	public static void clickOnNextButton()
	{
		CommonUtil.clickOnButton(nextButton);		
	}
	
	/**
	 * This is developed by nagendra
	 * enterDomainNameInTextbox is used to enter domain name in text box
	 * @param domainNameValue
	 * domainNameValue is the text which we need to enter in domain name text box
	 * This method return type is void
	 */
	public static void enterDomainNameInTextbox(String domainNameValue)
	{
			CommonUtil.enterTextInTextBox(domainNameTextbox, domainNameValue);

	}
	
	/**
	 * This is developed by nagendra
	 * selectDeploymentTypeFromDropdown is used to select option from Deployment type drop down
	 * @param option
	 * here option is visible text in deployment type dropdown
	 * This method return type is void
	 */
	public static void selectDeploymentTypeFromDropdown(String option)
	{
			CommonUtil.selectOptionFromDropdownByVisibleText(deploymentTypeSelector, option);
	}
	
	/**
	 * This is developed by nagendra
	 * enterforwardingIpAddressInTextbox is used to enter forwarding ip address in textbox
	 * @param forwardingIpAddressVale
	 * This method return type is void
	 */
	public static void enterforwardingIpAddressInTextbox(String forwardingIpAddressVale)
	{
			CommonUtil.enterTextInTextBox(forwardingIpAddressTextbox, forwardingIpAddressVale);

	}	
	
	/**
	 * This is developed by nagendra
	 * selctLeftHttp is used to select the check box of http of the left side.
	 * This method return type is void
	 */
	public static void selctLeftHttp()
	{
		CommonUtil.clickCheckBox(leftHttp);

	}
	
	/**
	 * This is developed by nagendra
	 * selctRightHttp is used to check box of http of the right side.
	 * This method return type is void
	 */
	public static void selctRightHttp()
	{
			CommonUtil.clickCheckBox(rightHttp);

	}
	
	/**
	 * This is developed by nagendra
	 * clickOnSubmitButton is used to click on Submit button
	 * This method return type is void
	 */
	public static void clickOnSubmitButton()
	{
			CommonUtil.clickOnButton(submitButton);
	}


	/**
	 * This is developed by nagendra
	 * validateDomainName is used to validate the domain in Dashbord whether the domain name is
	 * displyed or not
	 * @param expDomainName
	 * This method return type is void
	 */
	public static void validateDomainName(String expDomainName) {

    		String domain = Driver.driver.findElement(By.xpath("//div[@class='multisites-link']")).getText();
//    		String domain = domainName.getText();
    		boolean bol = domain.equalsIgnoreCase(expDomainName);
//    		boolean isAddWebsiteSectionDisplayed = CommonUtil.isElementDisplayed(addWebsiteSection);
    		Assert.assertTrue(bol,"Adding website failed");
	}
	
	/**
	 * This is developed by nagendra
	 * selctLeftHttps is used to select check of the left side https
	 * This method return type is void
	 */
	public static void selctLeftHttps()
	{
			CommonUtil.clickCheckBox(leftHttps);
	}
	
	/**
	 * This is developed by nagendra
	 * selctRightHttps is used to select check of the right side https
	 * This method return type is void
	 */
	public static void selctRightHttps()
	{
			CommonUtil.clickCheckBox(rightHttps);
	}
	
	
	/**
	 * This is developed by nagendra
	 * enterHttpsChainCertificateInTextbox is used to enter chain certificate in text box
	 * @param httpsChainCertificateValue
	 * This method return type is void
	 */
	public static void enterHttpsChainCertificateInTextbox(String httpsChainCertificateValue)
	{
		CommonUtil.enterTextInTextBox(httpsChainCertificate, httpsChainCertificateValue);
	}
	
	/**
	 * This is developed by nagendra
	 * enterHttpsPrivateKeyInTextbox is used to enter Private Key in text box
	 * @param httpsPrivateKeyValue
	 * This method return type is void
	 */
	public static void enterHttpsPrivateKeyInTextbox(String httpsPrivateKeyValue)
	{
			CommonUtil.enterTextInTextBox(httpsPrivateKey, httpsPrivateKeyValue);
	}
	
	/**
	 * This is developed by nagendra
	 * enterHttpsCertificateInTextbox is used to enter certificate in text box
	 * @param httpsCertificateValue
	 * This method return type is void
	 */
	public static void enterHttpsCertificateInTextbox(String httpsCertificateValue)
	{
			CommonUtil.enterTextInTextBox(httpsCertificate, httpsCertificateValue);
	}
	
	/**
	 * This is developed by nagendra
	 * enterPrivateKeyPassphraseInTextbox is used to enter private key passphrase in text box
	 * @param privateKeyPassphrase
	 * This method return type is void
	 */
	public static void enterPrivateKeyPassphraseInTextbox(String privateKeyPassphrase)
	{
			CommonUtil.enterTextInTextBox(httpsPrivateKeyPassphrase, privateKeyPassphrase);
	}	

	/**
	 * This is developed by nagendra
	 * clickSkipButtoon is used to click on skip button
	 * This method return type is void
	 */
	public static void clickOnSkipButtoon()
	{
			CommonUtil.clickOnButton(skipButton);
	}
	
	/**
	 * This is developed by bhanjan
	 * validateHealthSummaryPage is used to validate Health summary page
	 * @return
	 * This method return type is boolean
	 */
	
	public static boolean validateMultiDomainPage(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			String expectedText ="All Sites - Health Summary";
			String actualText =healthSummaryLabel.getText();
			Assert.assertTrue(actualText.contains(expectedText),"loading Multi Domain Page Failed...");
			log.info("Multi Domain Page launched Sucessfully...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
			}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * isRouteConfigreuire is used to check the route status befor do route config
	 * @return
	 * This method return type is boolean
	 */
	public static boolean isRouteConfigreuire(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			String expectedText ="Routing Configuration Required";
			String actualText =routeConfigRequiredLabel.getText();
			Assert.assertTrue(actualText.contains(expectedText),"Getting Webelement Failed...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
			}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * doRouteConfig is used to do route congfig
	 * This method return type is void
	 */
	public static void doRouteConfig(){
		try{
//			RouteConfig.createResourceRecords(RouteConfig.prepareCNAMERecordsReq("CREATE",RouteConfig.Name,RouteConfig.IP_ADDRESS));
			RouteConfig.createResourceRecords(RouteConfig.prepareCNAMERecordsReq("CREATE",CommonUtil.getPropertyValue("domainName"),RouteConfig.IP_ADDRESS));
			log.info("Create Resource Record ...");
//			RouteConfig.routeConfigResourceRecords(RouteConfig.prepareCNAMERecordsReq("UPSERT",RouteConfig.Name,RouteConfig.WAF_ADDRESS));
			RouteConfig.routeConfigResourceRecords(RouteConfig.prepareCNAMERecordsReq("UPSERT",CommonUtil.getPropertyValue("domainName"),RouteConfig.WAF_ADDRESS));
			log.info("Config Resource Record change has done ...");
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	/**
	 * This is developed by bhanjan
	 * checkConfiguredRoute is used to check the status after route config
	 * This method return type is void
	 */
	public static void checkConfiguredRoute()
	{
		for(int i=0;i<30;i++){
		Driver.driver.findElement(By.xpath("//img[@title='Refresh']")).click();
		log.info("clicked on refresh button");
		CommonUtil.explicitWait(60000);
		boolean displayed = MultiDomainPage.getConfigurationStatusByDomianName(CommonUtil.getPropertyValue("domainName"));
		
		log.info("Sleep for 60 second...");
		if(displayed){
			log.info("Website is configured to route traffic through Indusface Web Application Firewall");
			break;
		}
		}
	}
	
	/**
	 * This is developed by bhanjan
	 * isRoutingDone is used to call doRouteConfig and checkConfigRoute methods
	 * @return
	 * This method return type is boolean
	 */
	
	public static boolean isRoutingDone(Boolean val){
		
		try{
			if(!(val)){
				log.info("Doing route congiguration...");
				doRouteConfig();
				log.info("Checking route config status ...");
				checkConfiguredRoute();
			    log.info("RouteConfig has completed ...");
				return true;
			}else{
				log.info("RouteConfig has completed ...");
				return false;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	/**
	 * This is developed by nagendra
	 * getConfigurationStatusByDomianName is used to get the configuration status of the domain
	 * @param domainName
	 * domain is website which we need to check the configuration status even if we have multiple domain names
	 * @return
	 * This method return type is boolean
	 */
	public static boolean getConfigurationStatusByDomianName(String domainName) {

		String configurationStatusClass =  Driver.driver.findElement(By.xpath("//*[text()='"+domainName+"']/../../td[2]/*")).getAttribute("class");		
		if(configurationStatusClass.equals("gwt-Image"))
			return true;
		else 
			return false;
	}

	/**
	 * This is developed by nagendra
	 * getLastScanDetails is used to get the last scan details
	 * @return
	 * This method return type is String
	 */
	public static String getLastScanDetails() {
		String lastScanDetails = lastScanValue.getText();
    	log.info("Last scan details :  "+ lastScanDetails);
		return lastScanDetails;	
	}
	
	
	/**
	 * This is developed by nagendra
	 * clickOnScanNowButtonByDomanName is used to click on scan now button of the specified domain
	 * @param domainName
	 * This method return type is void
	 */
	public static void clickOnScanNowButtonByDomanName(String domainName) {

		Driver.driver.findElement(By.xpath("//*[text()='"+domainName+"']/../..//strong[text()='Scan Now']")).click();		
		
	}


	/**
	 * This is developed by nagendra
	 * clickOnContinueButton is used to click on continue button after scan is initiated
	 * This method return type is void
	 */
	public static void clickOnContinueButton() {
		
		 CommonUtil.clickOnButton(continueButton);		
	}
	
	/**
	 * This is developed by nagendra
	 * clickOnScanRequestSectionCloseButton is used to click on close button of the Scan initiated popup
	 * This method return type is void
	 */
	public static void clickOnScanRequestSectionCloseButton() {
		
		 CommonUtil.clickOnButton(scanRequestSectionCloseButton);		
	}
	
	/**
	 * This is developed by Joancy
	 * clickOnProfileLinkImage is used to click on profile link 
	 * This method return type is void
	 */
	public static void clickOnProfileLinkImage() {
		  CommonUtil.clickOnLink(profile);
		     log.info("click on Profile Link");
		 }
	/**
	 * This is developed by Bhanjan
	 * clickOnWebSiteLink is used to click the websitelink
	 * This method return type is void
	 */
	public static void clickOnWebSiteLink() {
		  CommonUtil.clickOnLink(webSiteLink);
		     log.info("click on webSiteLink");
		 }
	
	/**
	 * This is developed by nagendra
	 * clickOnWebSiteLinkByDomainName is used to click on domain name
	 * @param domainName
	 * This method return type is void
	 */
	public static void clickOnWebSiteLinkByDomainName(String domainName) {
		
		Driver.driver.findElement(By.xpath("//a[@class='multisites-link'][text()='"+domainName+"']")).click();
		log.info("clicked on Website link "+domainName);
		
	}
	
public static void clickOnIndusFaceLogo() {
		
	indusFaceLogo.click();
		log.info("clicked on IndusfaceLogo");
		
	}
	
}
