package com.igwaf.pages;

import javax.xml.xpath.XPath;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class DamnVulnerableWebApplicationPage {
	
	@FindBy(name="username")
	public static WebElement DamnVulnerableWebApplicationUserName;
	
	@FindBy(name="password")
	public static WebElement DamnVulnerableWebApplicationPassWord;
	
	@FindBy(name="Login")
	public static WebElement DamnVulnerableWebApplicationLogInButton;
	
	@FindBy(xpath="//a[text()='Home']")
	public static WebElement DamnVulnerableWebApplicationHomeTab;
	
	@FindBy(xpath="//a[text()='XSS reflected']")
	public static WebElement xssReflectedLink;
	
	@FindBy(xpath="//a[text()='SQL Injection']")
	public static WebElement sqlInjectionLink;
	
	@FindBy(name="name")
	public static WebElement xssfEditBox;
	
	@FindBy(name="id")
	public static WebElement sqlInjectionEditBox;
	
	@FindBy(xpath="//h2[contains(text(),'406 Something Malicious')]")
	public static WebElement maliciousHeader;
	
	@FindBy(xpath="//input[@value='Submit']")
	public static WebElement submitBtton;
	
	@FindBy(xpath="//a[text()='Logout']")
	public static WebElement logOutBtton;
	
	
	
	
	
	
	public static Logger log=Logger.getLogger(DamnVulnerableWebApplicationPage.class);
	
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, DamnVulnerableWebApplicationPage.class);
	}
	
	public static boolean doLogInToDamnVulnerableWebApplication(){
		try{
			String url=CommonUtil.getPropertyValue("domainName")+"/login.php";
			System.out.println(url);
			Driver.driver.navigate().to("http://"+url);
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
			DamnVulnerableWebApplicationUserName.sendKeys(CommonUtil.getPropertyValue("DWVA_UserName"));
			DamnVulnerableWebApplicationPassWord.sendKeys(CommonUtil.getPropertyValue("DWVA_PassWord"));
			DamnVulnerableWebApplicationLogInButton.click();
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	public static boolean doLogOutFromDamnVulnerableWebApplication(){
		try{
			CommonUtil.waitForVisibilityOfElement(logOutBtton);
			CommonUtil.clickOnLink(logOutBtton);
			log.info("Logout has done sucessfully...");
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	public static boolean validateDamnVulnerableWebApplicationPage(){
		try{
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
			boolean isDisplayedHomeTab = DamnVulnerableWebApplicationHomeTab.isDisplayed();
			
			Assert.assertTrue(isDisplayedHomeTab,"Home Tab is validate sucessfully...");
			log.info("Validate DamnVulnerableWebApplication Home page sucessfully... ");
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static boolean doXssAttack(){
		try{
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_HIGH);
			CommonUtil.clickOnLink(xssReflectedLink);
			CommonUtil.waitForVisibilityOfElement(xssfEditBox);
			CommonUtil.enterTextInTextBox(xssfEditBox,CommonUtil.getPropertyValue("XssfAttack"));
			CommonUtil.clickOnButton(submitBtton);
			CommonUtil.waitForVisibilityOfElement(maliciousHeader);
			boolean isElementDisplayed = CommonUtil.isElementDisplayed(maliciousHeader);
			Assert.assertTrue(isElementDisplayed,"xssf attacks has not protected till now...");
			log.info("Xssf attack has succesfully done...");
			CommonUtil.navigateBack();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static boolean doSqlInjectionAttack(){
		try{
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
			CommonUtil.waitForVisibilityOfElement(sqlInjectionLink);
			boolean isSqlLinkDisplayed = CommonUtil.isElementDisplayed(sqlInjectionLink);
			Assert.assertTrue(isSqlLinkDisplayed,"SQL injection  link has not display...");
			CommonUtil.clickOnLink(sqlInjectionLink);
			CommonUtil.waitForVisibilityOfElement(sqlInjectionEditBox);
			CommonUtil.enterTextInTextBox(sqlInjectionEditBox,CommonUtil.getPropertyValue("SqlAttack"));
			CommonUtil.clickOnButton(submitBtton);
			CommonUtil.waitForVisibilityOfElement(maliciousHeader);
			boolean isElementDisplayed = CommonUtil.isElementDisplayed(maliciousHeader);
			Assert.assertTrue(isElementDisplayed,"SqlInjection attacks has not protected till now...");
			log.info("SqlInjection attack has succesfully done...");
			CommonUtil.navigateBack();
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}
