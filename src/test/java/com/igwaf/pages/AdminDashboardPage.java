package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Driver;

public class AdminDashboardPage {
	
public static Logger log= Logger.getLogger(SignUpPage.class);

//-----------------------------------------------------------------------------                       
//						PAGE ELEMENTS
//----------------------------------------------------------------------------- 
	
	@FindBy(xpath="//table[@id='da-ex-datatable-default']")
	public static WebElement CustomersTabTable;
	
	@FindBy(xpath="//a[text()='Users ']")
	public static WebElement usersTab;
	
	@FindBy(xpath="//div[text()='All Users']")
	public static WebElement usersTabAllUsersLabel;
	
	@FindBy(xpath="//td/input")
	public static WebElement userNameInputbox;
	
	@FindBy(xpath="//a[text()='User Roles ']")
	public static WebElement userRolesTab;
	
	@FindBy(xpath="//div[text()='User Roles']")
	public static WebElement userRolesTabUserRolesLabel;
	
	@FindBy(xpath="//div[text()='Websites']")
	public static WebElement websitesTabWebsitesLabel;	
	
	@FindBy(xpath="(//div[text()='Edit'])[1]")
	public static WebElement editLink;
	
	@FindBy(xpath="//a[text()='Websites ']")
	public static WebElement websitesTab;
	
//-----------------------------------------------------------------------------                       
//								METHODS
//----------------------------------------------------------------------------- 
	
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, AdminDashboardPage.class);
	}
	 
	/**
	 * This is developed by nagendra
	 * validateCustomersTab is used to Validate the Customer Tab by verifying the Table is displayed or not
	 * This method return type is void
	 */
	public static void validateCustomersTab(){

		boolean isDashboardTableDisplayed = CommonUtil.isElementDisplayed(CustomersTabTable);
		Assert.assertTrue(isDashboardTableDisplayed,"Customers Tab is not displayed");
		log.info("Customers Tab is not displayed");	
	}
	
	/**
	 * This is developed by nagendra
	 * validateNameInCustomersTab is used to verify Name in Customer tab displayed or not
	 * @param userName
	 * This method return type is void
	 */
	public static void validateNameInCustomersTab(String userName){

			Boolean isCustomerDisplayed = Driver.driver.findElement(By.xpath("//td[text()='"+userName+"']")).isDisplayed();
			Assert.assertTrue(isCustomerDisplayed,"Customer is not displayed in Customers Tab");
			log.info("Customer is displayed in Customers Tab");
	}
	
	/**
	 * This is developed by nagendra
	 * navigateToUsersTab is used to Navigate to the Users tab clicking Users Tab
	 * This method return type is void
	 */
	public static void navigateToUsersTab(){

			CommonUtil.clickOnLink(usersTab);
	}	
	
	/**
	 * This is developed by nagendra
	 * validateUsersTab is used to Validate the Users tab by All Users label is displayed or not
	 * This method return type is void
	 */
	public static void validateUsersTab(){
	
    		boolean isUsersTabAllUsersLabelDisplayed = CommonUtil.isElementDisplayed(usersTabAllUsersLabel);
        	Assert.assertTrue(isUsersTabAllUsersLabelDisplayed,"Users Tab is not displayed");
        	log.info("Users Tab is displayed");	
        	}
	
	/**
	 * This is developed by nagendra
	 * enterTextInSearchTextboxOfUsersTab is used to enter email in Search Text box of Users Tab
	 * @param email
	 * This method return type is void
	 */
	public static void enterTextInSearchTextboxOfUsersTab(String email){
			userNameInputbox.clear();
			CommonUtil.enterTextInTextBox(userNameInputbox,email);	
	}
	
	/**
	 * This is developed by nagendra
	 * validateUserNameInUsersTab is used to where Email is displayed in User Table or not
	 * @param email
	 * This method return type is void
	 */
	public static void validateUserNameInUsersTab(String email){
		 email = email.split("@")[0];	
		 Boolean isUserNameDisplayed;
		try{
		isUserNameDisplayed = Driver.driver.findElement(By.xpath("//td[contains(text(),'"+email+"')]")).isDisplayed();
		System.out.println(isUserNameDisplayed);
		}catch(Exception e)
		{
			isUserNameDisplayed=false;
		}
//		d = Driver.driver.findElement(By.xpath("//td[text()='"+email+"']")).isDisplayed();
			Assert.assertTrue(isUserNameDisplayed,"User Name is not displayed in Users Tab");
			log.info("User Name is displayed in Users Tab");
	}
	
	/**
	 * This is developed by nagendra
	 * navigateToUserRolesTab is used to Navigate to the Users Roles tab clicking Users Roles Tab
	 * This method return type is void
	 */
	public static void navigateToUserRolesTab(){
			CommonUtil.clickOnLink(userRolesTab);
			}
	
	/**
	 * This is developed by nagendra
	 * validateUserRoleTab is used to Validate the User Roles Tab by verifying the All Users Label is displayed or not
	 * This method return type is void
	 */
	public static void validateUserRoleTab(){	
    		boolean isUsersTabAllUsersLabelDisplayed = CommonUtil.isElementDisplayed(userRolesTabUserRolesLabel);
        	Assert.assertTrue(isUsersTabAllUsersLabelDisplayed,"User Roles Tab is not displayed");
        	log.info("User Roles Tab is displayed");
	}

	/**
	 * This is developed by nagendra
	 * validateLoginInUserRoles is used to Validate whether email is displayed in User Roles or not
	 * @param email
	 * This method return type is void
	 */
	public static void validateLoginInUserRoles(String email){
		Boolean isLoginDisplayed = Driver.driver.findElement(By.xpath("//td[text()='"+email+"']")).isDisplayed();
		Assert.assertTrue(isLoginDisplayed,"login is not displayed in User Roles Tab");
		log.info("login is displayed in User Roles Tab");
	}
	
	/**
	 * This is developed by Joancy
	 * clickOnEditLink is used to click on Edit link
	 * This method return type is void
	 */
	public static void clickOnEditLink(){
		  CommonUtil.clickOnLink(editLink);
		  log.info("clicked on edit link of the all users table");
		 }
	
	/**
	 * This is developed by nagendra
	 * navigateToWebsitesTab is used to navigate to Websites tab
	 * This method return type is void
	 */
	public static void navigateToWebsitesTab(){
		CommonUtil.clickOnLink(websitesTab);
		}
	
	/**
	 * This is developed by nagendra
	 * validateDomainInWebsites is used to check whether the domain is displaying in Websites tab or not
	 * @param domain
	 * This method return type is void
	 */
	public static void validateDomainInWebsites(String domain){
		domain = domain.split(":")[0];
		Boolean isCustomerDisplayed = Driver.driver.findElement(By.xpath("//td[contains(text(),'"+domain+"'])")).isDisplayed();
		Assert.assertTrue(isCustomerDisplayed,"login is not displayed in User Roles Tab");
		log.info("login is displayed in User Roles Tab");
	}
}
