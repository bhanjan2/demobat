
package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class SettingsPage {


	public static Logger log= Logger.getLogger(SettingsPage.class);


	//-----------------------------------------------------------------------------                       
    //		PAGE ELEMENTS
	//----------------------------------------------------------------------------- 
	@FindBy(xpath="(.//input[@type='radio'])[2]")
	public static WebElement logonly;
	
	@FindBy(xpath="(.//input[@type='radio'])[1]")
	public static WebElement logandblock;
	
	@FindBy(xpath="(//img[@src='/dashboard/images/plus.png'])[1]")
	public static WebElement IPsBlacklisted;
	
	@FindBy(xpath="(//input[@type='text'])[2]")
	public static WebElement AddIPsBlacklisted;
	
	@FindBy(xpath="(//button[@type='button'])[4]")
	public static WebElement SaveIPsBlacklisted;
	
	@FindBy(xpath="(//img[@src='/dashboard/images/plus.png'])[2]")
	public static WebElement IPsWhitelisted;
	
	@FindBy(xpath="(//input[@type='text'])[3]")
	public static WebElement AddIPsWhitelisted;
	
	@FindBy(xpath="(//button[@type='button'])[5]")
	public static WebElement SaveIPsWhitelisted;
	
	@FindBy(xpath="(//img[@src='/dashboard/images/plus.png'])[3]")
	public static WebElement CountriesIPsBlacklisted;
	
	@FindBy(xpath="(//select[@class='gwt-ListBox'])[13]")
	public static WebElement AddCountriesSelectbox;
	
	@FindBy(xpath="(//button[@type='button'])[6]")
	public static WebElement SaveCountriesIPsBlacklisted;
	
	@FindBy(xpath="(//img[@src='/dashboard/images/plus.png'])[4]")
	public static WebElement URLsWhitelisted;
	
	@FindBy(xpath="(//input[@type='text'])[4]")
	public static WebElement AddURLsWhitelisted;
	
	@FindBy(xpath="(//button[@type='button'])[7]")
	public static WebElement SaveURLsWhitelisted;
	
	//-----------------------------------------------------------------------------                       
   //	PAGE METHODS
   //----------------------------------------------------------------------------- 
	/**
	 * This is developed by Joancy
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver,SettingsPage.class);
	}
	/**
	 * This is developed by joancy
	 * validateSettingsPage is used to 
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateSettingsPage()
	 {
	   try{
	    CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
	    if(CommonUtil.isAlertPresent()){
	     log.info("Found an Alert pop up...");
	     //CommonUtil.acceptAlert();
	     log.info("Click on ok button of  the Alert popup...");
	    }else{
	    String title = Driver.driver.getTitle();
	    String expectedText ="Settings";
	    Assert.assertTrue(title.contains(expectedText),"loading Settings page has failed... ");
	    log.info("SettingsPage launched Sucessfully...");
	    }
	   }catch(Exception e){
	    e.printStackTrace();
	    return false;
	   }
	   return true;
	  }
	/**
	 * This is developed by joancy
	 * validatetheLogOnlyradiobuttonofWAFstatus is used to validate the logonly radiobutton is selected or not
	 * This method return type is boolean
	 */
   public static Boolean validatetheLogOnlyradiobuttonofWAFstatus()
	{
		return CommonUtil.getcheckboxvalue(logonly);
	}
   /**
	 * This is developed by Joancy
	 * clickonlogandblockofWAFstatus is used to click on the radiobutton of logandblock
	 * This method return type is void
	 */
	public static void clickonlogandblockofWAFstatus()
	{
		CommonUtil.clickCheckBox(logandblock);
		log.info("checkbox is selected logandblock");
	}
	/**
	 * This is developed by joancy
	 * validatetheLogandBlockradiobuttonofWAFstatus is used to validate the logandblock radiobutton is selected or not
	 * This method return type is boolean
	 */
	public static Boolean validatetheLogandBlockradiobuttonofWAFstatus()
	{
		return CommonUtil.getcheckboxvalue(logandblock);
	}
	/**
	 * This is developed by Joancy
	 * clickonPlusofIPsBlacklisted is used to click on the plus button of IPsBlacklisted
	 * This method return type is void
	 */
	public static void clickonPlusofIPsBlacklisted()
	{
		CommonUtil.clickOnButton(IPsBlacklisted);
	}
	/**
	 * This is developed by Joancy
	 * enterIPsBlacklistintextbox is used to enter ipAddress in IPsBlacklist textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterIPsBlacklistintextbox(String ipAddress)
	{
	CommonUtil.enterTextInTextBox(AddIPsBlacklisted, ipAddress);
	}
	/**
	 * This is developed by Joancy
	 * saveIPsBlacklisted is used to click on Save button
	 * This method return type is void
	 */
	public static void saveIPsBlacklisted()
	{
		CommonUtil.clickOnButton(SaveIPsBlacklisted);
		log.info("IPs are Blacklisted");
	}
	/**
	 * This is developed by joancy
	 * validatetheIPsBlacklisted is used to validate the BlacklistedipAddress is saved successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static Boolean validatetheIPsBlacklisted(String ipAddress)
	{
		WebElement ipAddressele = Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']"));
		return ipAddressele.isDisplayed();
	}
	/**
	 * This is developed by joancy
	 * removetheIPsBlacklisted is used to remove the BlacklistedipAddress 
	 * @param ipAddress
	 * This method return type is void
	 */
	public static void removetheIPsBlacklisted(String ipAddress)
	{
		Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']/../..//a")).click();
		log.info(" Blacklisted IPs is Removed");
	}	
	/**
	 * This is developed by joancy
	 * validateremovedIPsBlacklisted is used to validate the BlacklistedipAddress is removed successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static  boolean validateremovedIPsBlacklisted(String ipAddress)
	{
		WebElement ipAddressele=Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']"));
		return ipAddressele.isDisplayed();
	}	
	 
	/**
	 * This is developed by Joancy
	 * clickonPlusofIPsWhitelisted is used to click on the plus button of IPsWhitelisted
	 * This method return type is void
	 */
	public static void clickonPlusofIPsWhitelisted()
	{
		CommonUtil.clickOnButton(IPsWhitelisted);
	}
	/**
	 * This is developed by Joancy
	 * enterIPsWhitelistedintextbox is used to enter ipAddress in IPsWhitelisted textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterIPsWhitelistintextbox(String ipAddress)
	{
		CommonUtil.enterTextInTextBox(AddIPsWhitelisted, ipAddress);
	}
	/**
	 * This is developed by Joancy
	 * saveIPsWhitelisted is used to click on Save button
	 * This method return type is void
	 */
	public static void saveIPsWhitelisted()
	{
		CommonUtil.clickOnButton(SaveIPsWhitelisted);
		log.info("IPs are Whitelisted");
	}
	/**
	 * This is developed by joancy
	 * validatetheIPsWhitelisted is used to validate the WhitelistedipAddress is saved successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static Boolean validatetheIPsWhitelisted(String ipAddress) 
	{
		WebElement ipAddressele = Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']"));
		return ipAddressele.isDisplayed();
	}
	/**
	 * This is developed by joancy
	 * removetheIPsWhitelisted is used to remove the WhitelistedipAddress 
	 * @param ipAddress
	 * This method return type is void
	 */
	public static void removetheIPsWhitelisted(String ipAddress)
	{
		Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']/../..//a")).click();
		log.info(" Whitelisted IPs is Removed");
	}
	/**
	 * This is developed by joancy
	 * validateremovedIPsWhitelisted is used to validate the WhitelistedipAddress is removed successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static Boolean validateremovedIPsWhitelisted(String ipAddress)
	{
		WebElement ipAddressele=Driver.driver.findElement(By.xpath("//div[text()='"+ipAddress+"']"));
		return ipAddressele.isDisplayed();
	}	
	/**
	 * This is developed by Joancy
	 * clickonPlusofCountriesIPsBlacklisted is used to click on the plus button of CountriesIPsBlacklisted
	 * This method return type is void
	 */
	public static void clickonPlusofCountriesIPsBlacklisted()
	{
		CommonUtil.clickOnButton(CountriesIPsBlacklisted);
	}
	/**
	 * This is developed by Joancy
	 * selectCountriesIPsBlacklistintextbox is used to select countryName in CountriesIPsBlacklisted textbox
	 * @param country
	 * This method return type is void
	 */
	public static void selectCountriesIPsBlacklistintextbox(String country)
	{
	CommonUtil.selectOptionFromDropdownByVisibleText(AddCountriesSelectbox, country);
	}
	/**
	 * This is developed by Joancy
	 * saveCountriesIPsBlacklisted is used to click on Save button
	 * This method return type is void
	 */
	public static void saveCountriesIPsBlacklisted()
	{
		CommonUtil.clickOnButton(SaveCountriesIPsBlacklisted);
		log.info("Countries IPs are Blacklisted");
	}
	/**
	 * This is developed by joancy
	 * validatetheCountriesIPsBlacklisted is used to validate the CountriesBlacklistedipAddress is saved successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static Boolean validatetheCountriesIPsBlacklisted(String country) 
	{
		WebElement countryname = Driver.driver.findElement(By.xpath("//div[text()='"+country+"']"));
		return countryname.isDisplayed();
	}
	/**
	 * This is developed by joancy
	 * removetheCountriesIPsBlacklisted is used to remove the CountriesBlacklistedipAddress 
	 * @param ipAddress
	 * This method return type is void
	 */
	public static void removetheCountriesIPsBlacklisted(String country)
	{
		Driver.driver.findElement(By.xpath("//div[text()='"+country+"']/../..//a")).click();
		log.info(" Countries Blacklisted IPs is Removed");
	}
	/**
	 * This is developed by joancy
	 * validateremovedCountriesIPsBlacklisted is used to validate the CountriesBlacklistedipAddress is removed successfully
	 * @param ipAddress
	 * This method return type is boolean
	 */
	public static Boolean validateremovedCountriesIPsBlacklisted(String country)
	{
		WebElement countryname=Driver.driver.findElement(By.xpath("//div[text()='"+country+"']"));
		return countryname.isDisplayed();
	}	
	/**
	 * This is developed by Joancy
	 * clickonPlusofURLsWhitelisted is used to click on the plus button of URLsWhitelisted
	 * This method return type is void
	 */
	public static void clickonPlusofURLsWhitelisted()
	{
		CommonUtil.clickOnButton(URLsWhitelisted);
	}
	/**
	 * This is developed by Joancy
	 * enterURLsWhitelistedintextbox is used to enter ipAddress in URLsWhitelisted textbox
	 * @param urls
	 * This method return type is void
	 */
	public static void enterURLsWhitelistintextbox(String urls)
	{
	CommonUtil.enterTextInTextBox(AddURLsWhitelisted, urls);
	}
	/**
	 * This is developed by Joancy
	 * saveURLsWhitelisted is used to click on Save button
	 * This method return type is void
	 */
	public static void saveURLsWhitelisted()
	{
		CommonUtil.clickOnButton(SaveURLsWhitelisted);
		log.info("URLs are Whitelisted");
	}
	/**
	 * This is developed by joancy
	 * validatetheURLsWhitelisted is used to validate the WhitelistedURLs is saved successfully
	 * @param urls
	 * This method return type is boolean
	 */
	public static Boolean validatetheURLsWhitelisted(String urls) 
	{
		WebElement url = Driver.driver.findElement(By.xpath("//div[text()='"+urls+"']"));
		return url.isDisplayed();
	}
	/**
	 * This is developed by joancy
	 * removetheURLsWhitelisted is used to remove the WhitelistedURLs
	 * @param urls
	 * This method return type is void
	 */
	public static void removetheURLsWhitelisted(String urls)
	{
		Driver.driver.findElement(By.xpath("//div[text()='"+urls+"']/../..//a")).click();
		log.info(" Whitelisted URLs is Removed");
	}
	/**
	 * This is developed by joancy
	 * validateremovedURLsWhitelisted is used to validate the WhitelistedURLs is removed successfully
	 * @param urls
	 * This method return type is boolean
	 */
	public static Boolean validateremovedURLsWhitelisted(String urls)
	{
		WebElement url=Driver.driver.findElement(By.xpath("//div[text()='"+urls+"']"));
		return url.isDisplayed();
	}	

	
}

