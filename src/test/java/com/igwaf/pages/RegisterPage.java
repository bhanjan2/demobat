package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class RegisterPage {

	
	public static Logger log= Logger.getLogger(RegisterPage.class);
	
	
//-----------------------------------------------------------------------------                       
//										PAGE ELEMENTS
//----------------------------------------------------------------------------- 
	
	@FindBy(xpath="//h1[text()='Indusface']")
	public static WebElement indusfaceLabel;
	
	@FindBy(id="fullName")
	public static WebElement fullNameInputbox;
	
	@FindBy(id="companyName")
	public static WebElement companyNameInputbox;
	
	@FindBy(id="email")
	public static WebElement emailInputbox;
	
	@FindBy(id="pass")
	public static WebElement passInputbox;
	
	@FindBy(id="planCode")
	public static WebElement planCodeInputbox;
	
	@FindBy(id="submitId")
	public static WebElement goButton;
	

//-----------------------------------------------------------------------------                       
//									PAGE METHODS
//----------------------------------------------------------------------------- 

	
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver,RegisterPage.class);
	}
	
	/**
	 * This is developed by nagendra
	 * enterFullNameInTextbox is used to Enter text into the full name textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterFullNameInTextbox(String value)
	{
		fullNameInputbox.clear();
		CommonUtil.enterTextInTextBox(fullNameInputbox, value);
	}
	
	/**
	 * This is developed by nagendra
	 * enterCompanyNameInTextbox is used to Enter text into the Company name textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterCompanyNameInTextbox(String value)
	{
		companyNameInputbox.clear();
		CommonUtil.enterTextInTextBox(companyNameInputbox, value);
	}
	
	/**
	 * This is developed by nagendra
	 * enterEmailInTextbox is used to  Enter text into the email textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterEmailInTextbox(String value)
	{
		emailInputbox.clear();
		CommonUtil.enterTextInTextBox(emailInputbox, value);
	}
	
	/**
	 * This is developed by nagendra
	 * enterpassInTextbox is used to  Enter text into the password textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterpassInTextbox(String value)
	{
		passInputbox.clear();
		CommonUtil.enterTextInTextBox(passInputbox, value);
	}
	
	/**
	 * This is developed by nagendra
	 * enterPlanCodeInTextbox is used to  Enter text into the plan code textbox
	 * @param value
	 * This method return type is void
	 */
	public static void enterPlanCodeInTextbox(String value)
	{
		planCodeInputbox.clear();
		CommonUtil.enterTextInTextBox(planCodeInputbox, value);
	}
	
	/**
	 * This is developed by nagendra
	 * clickGoButton is used to click on Go button
	 * This method return type is void
	 */
	public static void clickGoButton()
	{
		CommonUtil.clickOnButton(goButton);
	}

	/**
	 * This is developed by nagendra
	 * registerCustomer is used to register the customer using register.html page
	 * This method return type is void
	 */
	public static void registerCustomer() {

		enterFullNameInTextbox(CommonUtil.getPropertyValue("fullName"));
		enterCompanyNameInTextbox(CommonUtil.getPropertyValue("companyName"));
		enterEmailInTextbox(CommonUtil.getPropertyValue("emailId"));
		enterpassInTextbox(CommonUtil.getPropertyValue("password"));
		enterPlanCodeInTextbox(CommonUtil.getPropertyValue("plancode"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		clickGoButton();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	}
	
	
	
}
