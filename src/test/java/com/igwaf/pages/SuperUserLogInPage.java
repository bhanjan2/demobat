package com.igwaf.pages;



import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class SuperUserLogInPage  {

	public static Logger log= Logger.getLogger(SuperUserLogInPage.class);	
	
	
	@FindBy(id="da-login-username")
	private static WebElement usernameTextbox;
	
	@FindBy(id="da-login-password")
	private static WebElement passwordTextbox;
	
	@FindBy(id="da-login-submit")
	private static WebElement loginButton;
	
	public static SuperUserHomePage doLogin(String userName,String passWord){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			usernameTextbox.sendKeys(userName);
			passwordTextbox.sendKeys(passWord);
			loginButton.click();
			Thread.sleep(Constants.EXPLICIT_WAIT_HIGH);
			
		}catch(Exception e){
			e.printStackTrace();
			
			}
		return PageFactory.initElements(Driver.driver, SuperUserHomePage.class);
		
	}
	public boolean validateLoginPage(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			String expectedText ="Login : Indusface Total Application Security";
			//String text=CommonUtil.getTitle();
			String actualText =CommonUtil.getTitle();
			Assert.assertTrue(actualText.contains("Login : Indusface"),"loading LoginPage Failed...");
			log.info("LoginPage launched Sucessfully...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
			}
		return true;
	}
	
}
