package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Driver;

public class DetectPage {
	
	public static Logger log=Logger.getLogger(DetectPage.class);

	@FindBy(xpath="//span[@class='label label-danger']/div")
	public static WebElement criticalCount;
	
	@FindBy(xpath="//span[@class='label label-warning']/div")
	public static WebElement highCount;
	
	@FindBy(xpath="//span[@class='label label-info']/div")
	public static WebElement mediumCount;
	
	@FindBy(xpath="//div[@class='gwt-Label'][contains(text(),'Last Scan Request:')]")
	public static WebElement scanInProgress;
	
	@FindBy(xpath="//*[@class='active']//b")
	public static WebElement activeTab;
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver,DetectPage.class);
	}
	
	/**
	 * This is developed by nagendra
	 * getCriticalCount is used to get the count of Critical attacks
	 * @return
	 * This method return type is String
	 */
	public static String getCriticalCount(){

		return CommonUtil.getText(criticalCount);
	}
	
	/**
	 * This is developed by nagendra
	 * getHighCount is used to get the count of High attacks
	 * @return
	 * This method return type is String
	 */
	public static String getHighCount(){

		return CommonUtil.getText(highCount);
	}
	
	/**
	 * This is developed by nagendra
	 * getMediumCount is used to get the count of Medium attacks
	 * @return
	 * This method return type is String
	 */
	public static String getMediumCount(){

		return CommonUtil.getText(mediumCount);
	}
	
	/**
	 * This is developed by nagendra
	 * isScanInProgress is used to get the Scan in progress or not if scan in progress returns false
	 * otherwise it returns true
	 * @return
	 * This method return type is String
	 */
	public static Boolean isScanInProgress(){
		Boolean isDisplayed;
		try{
			isDisplayed = scanInProgress.isDisplayed();
		}
		catch(Exception e)
		{
			return false;
		}
		return isDisplayed;
	}
	
	/**
	 * This is developed by nagendra
	 * getActiveTab is used to Get the active tab Text
	 * @return
	 * This method return type is String
	 */
	public static String getActiveTab(){
		String activeTabText =  activeTab.getText();
		return activeTabText;
	}
}
