package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.igwaf.util.Driver;

public class WAFLoginPage {
	
	public static Logger log= Logger.getLogger(SignUpPage.class);

//-----------------------------------------------------------------------------                       
//								PAGE ELEMENTS
//----------------------------------------------------------------------------- 
		
	@FindBy(id="da-login-username")
	public static WebElement usernameTextbox;
	
	@FindBy(id="da-login-password")
	public static WebElement passwordTextbox;
	
	@FindBy(id="da-login-submit")
	public static WebElement loginButton;
	
//-----------------------------------------------------------------------------                       
//									PAGE METHODS
//----------------------------------------------------------------------------- 
	
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, WAFLoginPage.class);
	}

}
