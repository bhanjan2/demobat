package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class ProfilePage {
	
	public static Logger log= Logger.getLogger(ProfilePage.class);


    //-----------------------------------------------------------------------------                       
    //		PAGE ELEMENTS
    //----------------------------------------------------------------------------- 
		
		
		
		@FindBy(xpath="//div[@class='col-sm-11']")
		public static WebElement profileandlicenseinfo;

		@FindBy(xpath="//a[text()='Edit']")
		public static WebElement editLink;

		@FindBy(xpath="(//input[@class='gwt-TextBox'])[1]")
		public static WebElement nameTextbox;
		
		@FindBy(xpath="(//input[@class='gwt-TextBox'])[2]")
		public static WebElement emailIdTextbox;
		
		@FindBy(xpath="(//input[@class='gwt-TextBox'])[3]")
		public static WebElement phonenumberTextbox;
		
		@FindBy(xpath="//button[text()='Save']")
		public static WebElement saveButton;
		
		@FindBy(xpath="//div[@class='dlg-msg']")
		public static WebElement successMessageSectionTextinEdit;
		
		@FindBy(xpath="//img[@src='/assets/images/close_new.png']")
		public static WebElement closeButtonOfSucessSecioninEdit;
		
		@FindBy(xpath="(//a[text()='Change Password'])")
	    public static WebElement changepassword;

		@FindBy(xpath="(//input[@class='GNN-SDDCAP'])[1]")
		public static WebElement currentpassword;
		
		@FindBy(xpath="(//input[@class='GNN-SDDCAP'])[2]")
		public static WebElement newpassword;
		
		@FindBy(xpath="(//input[@class='GNN-SDDCAP'])[3]")
		public static WebElement confirmpassword;
		
	    @FindBy(xpath="//button[text()='Change']")
	    public static WebElement changebutton;
	    
		@FindBy(xpath="//div[@class='dlg-msg']")
		public static WebElement successMessageSectionTextinChangePassword;
		
		@FindBy(xpath="//img[@src='/assets/images/close_new.png']")
		public static WebElement closeButtonOfSucessSecioninChangePassword;
		
		@FindBy(xpath="//input[@type='checkbox']")
		public static WebElement dailysummaryreport;
		
		@FindBy(xpath="(//table)[5]/table[1]//tr[2]//td[2]")
		public static WebElement fullname;
		
     //-----------------------------------------------------------------------------                       
     //										PAGE METHODS
     //----------------------------------------------------------------------------- 

		/**
		 * This is developed by Joancy
		 * assignTothisPage is used to Sync with the opened browser with this class
		 * This method return type is void
		 */
		public static void assignTothisPage()
		{
			PageFactory.initElements(Driver.driver,ProfilePage.class);
		}
		
		/**
		 * This is developed by joancy
		 * validateProfilePage is used to 
		 * @return
		 * This method return type is boolean
		 */
		public static boolean validateProfilePage()
		{
			try{
			    CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			    if(CommonUtil.isAlertPresent()){
			     log.info("Found an Alert pop up...");
			     //CommonUtil.acceptAlert();
			     log.info("Click on ok button of  the Alert popup...");
			    }else{
					CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
					String title = Driver.driver.getTitle();
					System.out.println(title);
					String expectedText ="Profile";
					Assert.assertTrue(title.contains(expectedText),"loading Profile page has failed... ");
					log.info("ProfilePage launched Sucessfully...");
			    }
				}catch(Exception e){
					e.printStackTrace();
					return false;
				}
				return true;
			}
		/**
		 * This is developed by joancy
		 * validatetheusername is used to validate the userName is displayed or not
		 *  @param userName
		 * This method return type is boolean
		 */
		public static boolean validatetheusername(String userName)
		{
			WebElement userNameele=Driver.driver.findElement(By.xpath("//div[text()='"+userName+"']"));
			return userNameele.isDisplayed();
		}
		/**
		 * This is developed by Joancy
		 * clickOnEditLink is used to click on edit link
		 * This method return type is void
		 */
		public static void clickOnEditLink()
		{
			CommonUtil.clickOnLink(ProfilePage.editLink);
		}
		/**
		 * This is developed by Joancy
		 * enterNameInTextbox is used to enter name in Name textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enterNameInTextbox(String value)
		{
			nameTextbox.clear();
			CommonUtil.enterTextInTextBox(nameTextbox, value);
		}
		/**
		 * This is developed by Joancy
		 * enteremailIdInTextbox is used to to enter emailid in email textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enteremailIdInTextbox(String value)
		{
			emailIdTextbox.clear();
			CommonUtil.enterTextInTextBox(emailIdTextbox, value);
		}
		/**
		 * This is developed by Joancy
		 * phonenoInTextbox is used to to enter phonenumber in email textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enterphonenumberInTextbox(String value)
		{
			phonenumberTextbox.clear();
			CommonUtil.enterTextInTextBox(phonenumberTextbox, value);
		}
		/**
		 * This is developed by Joancy
		 * clickOnSaveButtonofEdit is used to click on Save button
		 * This method return type is void
		 */
		public static void clickOnSaveButtonofEdit()
		{
			CommonUtil.clickOnButton(saveButton);
		}
		/**
		 * This is developed by Joancy
		 * successmessageofEdit is used to display success messae of the userlogindetails
		 * This method return type is void
		 */
		public static void successmessageofEdit()
		{
		    Driver.driver.findElement(By.xpath("//div[text()='User login details updated successfully.']"));
		    log.info("successmessageofEdit is displayed");
		}
		/**
		 * This is developed by Joancy
		 * clickOnCloseButtonOfSucessSectioninEdit is used to click on close button of success message section
		 * This method return type is void
		 */
		public static void clickOnCloseButtonOfSucessSectioninEdit()
		{
			CommonUtil.clickOnButton(closeButtonOfSucessSecioninEdit);
			 log.info("successsection of user login has been closed");
		}
		/**
		 * This is developed by Joancy
		 * clickonChangePassword is used to click on the changepassword
		 * This method return type is void
		 */
		public static void clickonChangePassword()
		{
			CommonUtil.clickOnLink(changepassword);
		}
		/**
		 * This is developed by Joancy
		 * enterCurrentPasswordInTextbox is used to enter currentpassword in CurrentPassword textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enterCurrentPasswordInTextbox(String value)
		{
			CommonUtil.enterTextInTextBox(currentpassword, value);
		}
		/**
		 * This is developed by Joancy
		 * enterNewPasswordInTextbox is used to enter newpassword in NewPassword textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enterNewPasswordInTextbox(String value)
		{
			CommonUtil.enterTextInTextBox(newpassword, value);
		}
		/**
		 * This is developed by Joancy
		 * enterConfirmPasswordInTextbox is used to enter confirmpassword in ConfirmPassword textbox
		 * @param value
		 * This method return type is void
		 */
		public static void enterConfirmPasswordInTextbox(String value)
		{
			CommonUtil.enterTextInTextBox(confirmpassword, value);
		}
		/**
		 * This is developed by Joancy
		 * clickonChangeButtonofChangePassword is used to click on the changebutton of ChangePassword
		 * This method return type is void
		 */
		public static void clickonChangeButtonofChangePassword()
		{
			CommonUtil.clickOnButton(changebutton);
		}
		/**
		 * This is developed by Joancy
		 * successmessageofChangePassowrd is used to display the success message of ChangePassword
		 * This method return type is void
		 */
		public static void successmessageofChangePassowrd()
		{
		    Driver.driver.findElement(By.xpath("//div[text()='Password changed Successfully.']"));
		    log.info("successmessageofChangePassoword is displayed");
		}
		/**
		 * This is developed by Joancy
		 * clickOnCloseButtonOfSucessSectioninChangePassword is used to click on the closebutton of SucessSectioninChangePassword
		 * This method return type is void
		 */
		public static void clickOnCloseButtonOfSucessSectioninChangePassword()
		{
			CommonUtil.clickOnButton(closeButtonOfSucessSecioninChangePassword);
			 log.info("successsection of change password has been closed");
		}
		/**
		 * This is developed by Joancy
		 * clickonDailySummaryReport is used to click on the checkbox of dailysummaryreport
		 * This method return type is void
		 */
		public static void clickonDailySummaryReport()
		{
			CommonUtil.clickCheckBox(dailysummaryreport);
		}
		/**
		 * This is developed by joancy
		 * validatetheDailySummaryReport is used to validate the dailysummaryreport checkbox is selected or not
		 * This method return type is boolean
		 */
		public static Boolean validatetheDailySummaryReport()
		{
			return CommonUtil.getcheckboxvalue(dailysummaryreport);
		}
		
}


