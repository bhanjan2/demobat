package com.igwaf.pages;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;


import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;

public class SuperUserHomePage {
	public static Logger log= Logger.getLogger(SuperUserHomePage.class);
	
	
	@FindBy(xpath="//img[@class='da-header-button-logout']")
	private static WebElement logoutBtn;
	
	@FindBy(xpath="//a[text()='Users ']")
	private static WebElement usersTab;
	
	@FindBy(xpath="//div[text()='All Users']")
	private static WebElement allUsersLabel;
	
	@FindBy(xpath="//div[text()='Add New User']")
	private static WebElement addNewUserLink;
	
	@FindBy(xpath="(.//div[@id='da-content-area']//input)[1]")
	private static WebElement userNameTextbox;
	
	@FindBy(xpath="(.//div[@id='da-content-area']//input)[3]")
	private static WebElement passwordTextbox;
	
	@FindBy(xpath="(.//div[@id='da-content-area']//input)[4]")
	private static WebElement fullNameTextbox;

	@FindBy(xpath="(.//div[@id='da-content-area']//input)[5]")
	private static WebElement emailTextbox;
	
	@FindBy(xpath="(.//div[@id='da-content-area']//input)[6]")
	private static WebElement contactTextbox;
	
	@FindBy(xpath="//button[@class='save-btn']")
	private static WebElement saveButton;
	
	@FindBy(xpath="//colgroup[1]/..//tr/td[1]")
	private static List<WebElement> userName;
	
	public boolean validateHomePage(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			String expectedText ="Customers";
//			String actualText =CommonUtil.getText("//div[@class='grid-title']");
//			Assert.assertTrue(actualText.contains(expectedText),"loading HomePage Failed...");
			log.info("HomePage launched Sucessfully...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
			}
		return true;
	}
	public boolean doLogout(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			logoutBtn.click();
			log.info("Logout Sucessfully...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
			}
		return true;
	}
	
	public void navigateToUsersTab(){
		
		usersTab.click();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		log.info("Navigating to Users Tab");
	}
	
	public void validateNavigationToUsersTab()
	{
		
	boolean isNavigationToUsersTab = allUsersLabel.isDisplayed();
	Assert.assertTrue(isNavigationToUsersTab,"Validate SuperUserLogInPage Failed...");
	log.info("Navigated to Users Tab sucessfully");
	} 
	
public void clickAddNewUserLink(){
	
	addNewUserLink.click();
	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	log.info("clicked on Add New UserLink");
}

public void enterUserNameToAddNewUser(String userName){
	
	userNameTextbox.sendKeys(userName);
	log.info("Entered user name Value in Textbox");
}

public void enterPasswordToAddNewUser(String password){
	
	passwordTextbox.sendKeys(password);
	log.info("Entered password Value in Textbox");
}

public void enterFullNameToAddNewUser(String fullName){
	
	fullNameTextbox.sendKeys(fullName);
	log.info("Entered FullName Value in Textbox");
}

public void enterEmailToAddNewUser(String email){
	
	emailTextbox.sendKeys(email);
	log.info("Entered email Value in Textbox");
}

public void entercontactToAddNewUser(String contact){
	
	contactTextbox.sendKeys(contact);
	log.info("Entered contact Value in Textbox");
}

public void clickSaveButton(){
	
	saveButton.click();
	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	log.info("Clicked on Save button");
}

public Boolean validateCreatedUserNameInUsersTab(String userName1){
	
	Iterator<WebElement> userNameIterator = userName.iterator();
	while(userNameIterator.hasNext())
	{
		if((userNameIterator.next().getText()).equalsIgnoreCase(userName1))
		{
			return true;
		}
	}
	return false;
	} 


}
