package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;

public class SignUpPage {
	
	public static Logger log= Logger.getLogger(SignUpPage.class);
	
//	_______________________________________________________________________________________
//   							PAGE ELEMENTS
//_______________________________________________________________________________________
	
	@FindBy(xpath="//body/div[2]/div")
	public static WebElement signUpLabel;
	
	@FindBy(name="extCustId")
	public static WebElement awsCustomerId;

	@FindBy(name="promoCode")
	public static WebElement promoCode;
	
	@FindBy(name="nameOfTheUser")
	public static WebElement fullName;
	
	@FindBy(name="userEmail")
	public static WebElement emailId;
	
	@FindBy(name="password")
	public static WebElement password;
	
	@FindBy(name="rpassword")
	public static WebElement rePassword;	
	
	@FindBy(name="customerName")
	public static WebElement companyName;
	
	@FindBy(name="phoneNo")
	public static WebElement phoneNo;
		
	@FindBy(xpath="//input[@value='REGISTER']")
	public static WebElement registerButton;
		
	/**
	 * This is developed by nagendra
	 * signUp is used to Register the user 
	 * @throws Exception
	 * This method return type is void
	 */
	public void signUp() throws Exception
	{
	CommonUtil.isElementDisplayed(SignUpPage.signUpLabel);
	CommonUtil.enterTextInTextBox(SignUpPage.awsCustomerId, CommonUtil.getPropertyValue("awsCustomerId"));
	CommonUtil.enterTextInTextBox(SignUpPage.promoCode, CommonUtil.getPropertyValue("promoCode"));
	CommonUtil.enterTextInTextBox(SignUpPage.fullName, CommonUtil.getPropertyValue("fullName"));
	CommonUtil.enterTextInTextBox(SignUpPage.emailId, CommonUtil.getPropertyValue("emailId"));
	CommonUtil.enterTextInTextBox(SignUpPage.password, CommonUtil.getPropertyValue("password"));
	CommonUtil.enterTextInTextBox(SignUpPage.rePassword, CommonUtil.getPropertyValue("password"));
	CommonUtil.enterTextInTextBox(SignUpPage.companyName, CommonUtil.getPropertyValue("companyName"));
	CommonUtil.enterTextInTextBox(SignUpPage.phoneNo, CommonUtil.getPropertyValue("phoneNo"));
	CommonUtil.clickOnButton(SignUpPage.registerButton);
	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	UserLoginPage.assignTothisPage();
	Boolean loginLabelDeisplayed = CommonUtil.isElementDisplayed(UserLoginPage.loginLabel);
	Assert.assertTrue(loginLabelDeisplayed,"Not navigated to Login Page");
	log.info("Login Label is displayed");
	}
}
