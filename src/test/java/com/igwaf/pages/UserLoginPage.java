package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.igwaf.util.Driver;

public class UserLoginPage {
	
	public static Logger log= Logger.getLogger(SignUpPage.class);

//-----------------------------------------------------------------------------                       
//							PAGE ELEMENTS
//----------------------------------------------------------------------------- 
	
	@FindBy(xpath="//div[@class='gwt-Label']")
	public static WebElement loginLabel;
	
	@FindBy(id="da-login-username")
	public static WebElement userName;
	
	@FindBy(id="da-login-password") 
	public static WebElement password;
	
	@FindBy(id="da-login-submit")
	public static WebElement singInButton;
	
//-----------------------------------------------------------------------------                       
//								PAGE METHODS
//----------------------------------------------------------------------------- 
	
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, UserLoginPage.class);
	}
	
}
