package com.igwaf.pages;

import java.util.LinkedHashSet;
import java.util.List;

import org.apache.bcel.classfile.Constant;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class SummaryPage {
	public static Logger log=Logger.getLogger(SummaryPage.class);
	
	@FindBy(xpath="//span/div[text()='0']")
	public static WebElement attacksCount;
	
	@FindBy(xpath="//li//b[text()='Detect']/..")
	public static WebElement detectTab;
	
	@FindBy(xpath="//li//b[text()='Protect']/..")
	public static WebElement protectTab;
	
	@FindBy(xpath="//img[@src='/dashboard/images/settings.png']")
	public static WebElement settings;
	
	@FindBy(xpath="//li//b[text()='Summary']/..")
	public static WebElement summaryTab;
	
	@FindBy(xpath="//div[@class='jumbotron']/div[4]/div[2]/label/span/div")
	public static WebElement totalBlockedAttacksCount;
	               
	@FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div[2]/table/tbody/tr/td[1]")
	public static List<WebElement> listOfIPsAddressInSummaryPage;
	
	@FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[3]/div/div[2]/table/tbody/tr/td[1]")
	public static List<WebElement> listOfUrlsInSummaryPage;
	
	@FindBy(xpath="//table[@class='pie-legend-row-grid']/tbody/tr/td")
	public static List<WebElement> listOfCategoryInSummaryPage;
	
	@FindBy(xpath="//li//b[text()='Monitor']/..")
    public static WebElement monitorTab;
	
	@FindBy(xpath="//span/div[@class='ddosAttacksLbl']")
	public static WebElement DdosAttacksBlockedCount;
	
	@FindBy(xpath="//div/table/tbody/tr[1]/td/div/div/div[2][text()='No data']")
	public static WebElement topFiveAttackscategoryCountBeforeAttack;

    @FindBy(xpath="(//div[@class='gwt-Label'])[2]")
    public static WebElement TotalVulnerabilitiesDetectedCount;
    
    @FindBy(xpath="(//div[@class='gwt-Label'])[3]")
    public static WebElement TotalBlockedAgainstDetectedVulnerabilitiesCount;
    
    @FindBy(xpath="(//div[@class='gwt-Label'])[4]")
    public static WebElement TotalBlockedAttacksCount;
    
    @FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[2]/div/div[2]/table/tbody/tr/td[1]")
	public static List<WebElement> listOfCountryInSummaryPage;
    
    
    @FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div[2]/table/tbody/tr/td[2]")
	public static List<WebElement> listOfIPsAddressCountInSummaryPage;
    		
    @FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[2]/div/div[2]/table/tbody/tr/td[2]")
	public static List<WebElement> listOfCountryCountInSummaryPage;
    
    @FindBy(xpath=".//*[@id='da-content']/div/div[2]/div/div[2]/div/div/div[2]/div[3]/div/div[2]/table/tbody/tr/td[2]")
	public static List<WebElement> listOfUrlsCountInSummaryPage;
    
    
    
    
    
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver,SummaryPage.class);
	}
	
	/**
	 * This is developed by bhanjan
	 * validateSummaryPage is used to validate Summary page
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateSummaryPage(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			if(CommonUtil.isAlertPresent()){
			     log.info("Found an Alert pop up...");
			     //CommonUtil.acceptAlert();
			     log.info("Click on ok button of  the Alert popup...");
			    }else{
			String title = Driver.driver.getTitle();
			String expectedText ="Summary";
			Assert.assertTrue(title.contains(expectedText),"loading summary page has failed... ");
			log.info("SummaryPage launched Sucessfully...");
			    }
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
		
	/**
	 * This is developed by bhanjan
	 * getTotalVulnerabilitiesDetectedCount is used to getTotalVulnerabilitiesDetectedCount
	 * @return
	 * This method return type is String
	 */
	public static String getTotalVulnerabilitiesDetectedCount(){
		String text=null;
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    text = TotalVulnerabilitiesDetectedCount.getText();
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return text;
	}
	
	/**
	 * This is developed by bhanjan
	 * getTotalBlockedAgainstDetectedVulnerabilitiesCount is used to getTotalBlockedAgainstDetectedVulnerabilitiesCount 
	 * @return
	 * This method return type is String
	 */
	public static String getTotalBlockedAgainstDetectedVulnerabilitiesCount(){
		String text=null;
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    text = TotalBlockedAgainstDetectedVulnerabilitiesCount.getText();
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return text;
	}
	
	/**
	 * This is developed by bhanjan
	 * getTotalBlockedAttacksCount is used to getTotalBlockedAttacksCount
	 * @return
	 * This method return type is String
	 */
	public static String getTotalBlockedAttacksCount(){
		String text=null;
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    text = TotalBlockedAttacksCount.getText();
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return text;
	}
	
	
	/**
	 * This is developed by bhanjan
	 * getTotalDDoSAttacksBlockedCount is used to getTotalDDoSAttacksBlockedCount
	 * @return
	 * This method return type is String
	 */
	public static String getTotalDDoSAttacksBlockedCount(){
		String text=null;
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    text = DdosAttacksBlockedCount.getText();
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return text;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByIPsTableDataBeforeAttack is used to validateTopFiveAttacksByIPsTableDataBeforeAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByIPsTableDataBeforeAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfIPsAddressInSummaryPage.size();i++){
		    	
		    	if(!(listOfIPsAddressInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfIPsAddressInSummaryPage.get(i).getText());
		    	}
		    }
		  int size=ls.size();
		  String val=String.valueOf(size);
		    Assert.assertEquals(val,CommonUtil.getPropertyValue("TotalTop5AttacksByIPsCountInSummaryPageBeforeAttack").toString()); 
			log.info(" Validate Top 5 Attacks By IPs Table has  no data before any attack");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByCountiresTableDataBeforeAttack is used to validateTopFiveAttacksByCountiresTableDataBeforeAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByCountiresTableDataBeforeAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfCountryInSummaryPage.size();i++){
		    	
		    	if(!(listOfCountryInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfCountryInSummaryPage.get(i).getText());
		    	}
		    }
		    int size=ls.size();
			String val=String.valueOf(size);
		    Assert.assertEquals(val,CommonUtil.getPropertyValue("TotalTop5AttacksByCountriesCountInSummaryPageBeforeAttack")); 
			log.info(" Validate Top 5 Attacks By Country Table has  no data before any attack");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByUrlsTableDataBeforeAttack is used to validateTopFiveAttacksByUrlsTableDataBeforeAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByUrlsTableDataBeforeAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfUrlsInSummaryPage.size();i++){
		    	
		    	if(!(listOfUrlsInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfUrlsInSummaryPage.get(i).getText());
		    	}
		    }
		    int size=ls.size();
			String val=String.valueOf(size);
		    Assert.assertEquals(val,CommonUtil.getPropertyValue("TotalTop5AttackedURIsCountInSummaryPageBeforeAttack")); 
			log.info(" Validate Top 5 Attacks By Urls Table has  no data before any attack");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByIPsTableDataAfterAttack is used to validateTopFiveAttacksByIPsTableDataAfterAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByIPsTableDataAfterAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfIPsAddressInSummaryPage.size();i++){
		    	
		    	if(!(listOfIPsAddressInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfIPsAddressInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("IPAddressDetailsInSummaryPageAfterAttack")),"IPs details validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By IPs details Table  data after attack");
			
			LinkedHashSet<String> lhs=new LinkedHashSet<String>();
		    for(int i=0;i<listOfIPsAddressCountInSummaryPage.size();i++){
		    	
		    	if(!(listOfIPsAddressCountInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfIPsAddressCountInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("IPAddressCountInSummaryPageAfterAttack")),"IPs count validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By IPs Count Table  data after attack");
			
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByCountryTableDataAfterAttack is used to validateTopFiveAttacksByCountryTableDataAfterAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByCountryTableDataAfterAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfCountryInSummaryPage.size();i++){
		    	
		    	if(!(listOfCountryInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfCountryInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("CountryDetailsInSummaryPageAfterAttack")),"Country details validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Country details Table  data after attack");
			
			LinkedHashSet<String> lhs=new LinkedHashSet<String>();
		    for(int i=0;i<listOfCountryCountInSummaryPage.size();i++){
		    	
		    	if(!(listOfCountryCountInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfCountryCountInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("CountryCountInSummaryPageAfterAttack")),"Country Count validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Country Count Table  data after attack");
			
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksByUrlsTableDataAfterAttack is used to validateTopFiveAttacksByUrlsTableDataAfterAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksByUrlsTableDataAfterAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfUrlsInSummaryPage.size();i++){
		    	
		    	if(!(listOfUrlsInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfUrlsInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("UrlsDetailsBySqlInjectionInSummaryPageAfterAttack")),"Urls details BySqlInjection validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Urls details BySqlInjection Table  data after attack");
			
			
			Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("UrlsDetailsByXssfInSummaryPageAfterAttack")),"Urls details ByXss validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Urls details ByXss Table  data after attack");
			
			LinkedHashSet<String> lhs=new LinkedHashSet<String>();
		    for(int i=0;i<listOfUrlsCountInSummaryPage.size();i++){
		    	
		    	if(!(listOfUrlsCountInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfUrlsCountInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("UrlsCountBySqlInjectionInSummaryPageAfterAttack")),"Urls Count BySqlInjection validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Urls Count BySqlInjection Table  data after attack");
			
			Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("UrlsCountByXssInSummaryPageAfterAttack")),"Urls Count By Xss validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks By Urls Count By Xss Table  data after attack");
			
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateTopFiveAttacksCategoriesTableDataAfterAttack is used to validateTopFiveAttacksCategoriesTableDataAfterAttack
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateTopFiveAttacksCategoriesTableDataAfterAttack(){
		try{
			
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
		    LinkedHashSet<String> ls=new LinkedHashSet<String>();
		    for(int i=0;i<listOfCategoryInSummaryPage.size();i++){
		    	
		    	if(!(listOfCategoryInSummaryPage.get(i).getText()).trim().isEmpty()){
		    		ls.add(listOfCategoryInSummaryPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("SqlInjectionAttackCategoriesInSummaryPageAfterAttack")),"SqlInjectionAttackCategories details  validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks SqlInjectionAttackCategories details Table  data after attack");
			
			
			Assert.assertTrue(ls.contains(CommonUtil.getPropertyValue("CrossSiteScriptingAttackCategoriesInSummaryPageAfterAttack")),"CrossSiteScriptingAttackCategories details  validation has failed in Summary Page after Attack..."); 
			log.info(" Validate Top 5 Attacks CrossSiteScriptingAttackCategories details  Table  data after attack");			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by nagendra
	 * clickOnDetectTab is used to click on Detect tab
	 * This method return type is void
	 */

	public static void clickOnDetectTab(){

		CommonUtil.clickOnButton(detectTab);
	}
	
	public static void clickOnProtectTab(){

		CommonUtil.clickOnButton(protectTab);
	}

	public static void clickOnSettingsLinkImage() 
	{
		try{
		CommonUtil.clickOnLink(settings);
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		}catch(UnhandledAlertException r)
		{
			Driver.driver.switchTo().alert().accept();
		}
		catch(Exception e)
		{
			CommonUtil.pressEnterThroughActions();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			CommonUtil.pressEnterThroughActions();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			CommonUtil.pressEnterThroughActions();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		}
	}
	
	/**
	 * This is developed by bhanjan
	 * clickOnSummaryTab is used to click on summary tab
	 * This method return type is void
	 */
	public static void clickOnSummaryTab(){
		CommonUtil.clickOnButton(summaryTab);
	}
	/**
     * This is developed by nagendra
     * clickOnMonitorTab is used to click on Monitor tab
     * This method return type is void
     */
    public static void clickOnMonitorTab(){

        CommonUtil.clickOnButton(monitorTab);
    }

}
