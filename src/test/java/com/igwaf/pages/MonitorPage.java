package com.igwaf.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.igwaf.util.CommonUtil;
import com.igwaf.util.Driver;

public class MonitorPage {
	
	public static Logger log=Logger.getLogger(MonitorPage.class);
	
//-----------------------------------------------------------------------------                       
//											PAGE ELEMENTS
//----------------------------------------------------------------------------- 



	@FindBy(xpath="//span[@class='label label-danger']/div")
	public static WebElement customRulesCount;
	
	@FindBy(xpath="//span[@class='label label-warning']/div")
	public static WebElement totalPOCsCount;
	
	@FindBy(xpath="//span[@class='label label-info']/div")
	public static WebElement vulnerabilitiesCount;
	
	@FindBy(xpath="((//div[@class='dataTables_wrapper'])[1]//table)[1]//tr[2]")
	public static WebElement attacksByIpTableWithoutData;
	
	@FindBy(xpath="//*[@class='active']//b")
	public static WebElement activeTab;
	
//-----------------------------------------------------------------------------                       
//											PAGE METHODS
//----------------------------------------------------------------------------- 
		
		
	/**
	 * This is developed by nagendra
	 * assignTothisPage is used to Sync with the opened browser with this class
	 * This method return type is void
	 */
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver,MonitorPage.class);
	}
	
	/**
	 * This is developed by nagendra
	 * getCriticalCount is used to get the count of Custom Rules
	 * @return
	 * This method return type is String
	 */
	public static String getCustomRulesCount(){

		return CommonUtil.getText(customRulesCount);
	}
	
	/**
	 * This is developed by nagendra
	 * getHighCount is used to get the count of Total POCs Count
	 * @return
	 * This method return type is String
	 */
	public static String getTotalPOCsCount(){

		return CommonUtil.getText(totalPOCsCount);
	}
	
	/**
	 * This is developed by nagendra
	 * getMediumCount is used to get the count of Vulnerabilities
	 * @return
	 * This method return type is String
	 */
	public static String getVulnerabilitiesCount(){

		return CommonUtil.getText(vulnerabilitiesCount);
	}
	
	/**
	 * This is developed by nagendra
	 * validateAttacksByIpTableWithoutData is used to know whether the table is displayed with data or not
	 * if data is available it returns true if NoSuchElement is exception is raised then it returns false
	 * @return
	 * This method return type is Boolean
	 */
	public static Boolean validateAttacksByIpTableWithoutData(){
		Boolean value;
		try{
		value =  attacksByIpTableWithoutData.isDisplayed();
		}catch(Exception e)
		{
			return false;
		}
		return value;
	}	
	
	/**
	 * This is developed by nagendra
	 * getActiveTab is used to Get the active tab Text
	 * @return
	 * This method return type is String
	 */
	public static String getActiveTab(){
		String activeTabText =  activeTab.getText();
		return activeTabText;
	}	
}
