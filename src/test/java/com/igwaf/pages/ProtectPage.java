package com.igwaf.pages;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class ProtectPage {
	public static Logger log=Logger.getLogger(ProtectPage.class);
	
	@FindBy(xpath="//span[@title='Critical attacks blocked']/div")
	public static WebElement criticalCountInProtectPage;
	
	@FindBy(xpath="//span[@title='High category attacks blocked']/div")
	public static WebElement highCountInProtectPage;
	
	@FindBy(xpath="//span[@title='Medium category attacks blocked']/div")
	public static WebElement mediumCountInProtectPage;
	
	@FindBy(xpath="//a[@class='attacks-ip-tab']")
	public static WebElement attacksByIPTab;
	
	@FindBy(xpath="//a[@class='attacks-category-tab']")
	public static WebElement attacksByCategoryTab;	
	
	@FindBy(xpath="//b[text()='Attacks by URI']")
	public static WebElement attacksByUrltab;
	
	@FindBy(xpath="//input[@class='gwt-TextBox']")
	public static WebElement searchBox;
	
	@FindBy(xpath="//div[@id='da-ex-datatable-numberpaging_info']/div")
	public static WebElement listOfIPAddressBeforeAttack;
	
	@FindBy(xpath="//*[@class='active']//b")
	public static WebElement activeTab;
	
	@FindBy(xpath="((//div[@class='dataTables_wrapper'])[1]//table)[1]//tr[2]")
	public static WebElement attacksByIpTableWithoutData;
	
	@FindBy(xpath="//table[@class='table']/tbody/tr[2]/td[1]")
	public static WebElement firstRowofAttacksTableContents;
	
	@FindBy(xpath="(//div[@aria-label='A chart.'])[1]/*[1]/*[4]/*/*[2]/*[1][text()='SQL']")
	public static WebElement sqlInjectionInAttackCategoriesChart;
	
	@FindBy(xpath="(//div[@aria-label='A chart.'])[1]/*[1]/*[4]/*/*[2]/*[1][text()='Cross-']")
	public static WebElement crossSiteScriptingInAttackCategoriesChart;
	
	@FindBy(xpath="(//div[@aria-label='A chart.'])[2]/*[1]/*[4]/*/*[2]/*[1][text()='High']")
	public static WebElement highInAttackSeveritiesChart;
	
	@FindBy(xpath="(//div[@aria-label='A chart.'])[2]/*[1]/*[4]/*/*[2]/*[1][text()='Critical']")
	public static WebElement criticalInAttackSeveritiesChart;

	@FindBy(xpath="//table[@class='table']/tbody/tr/td[1]")
	public static List<WebElement> listOfIPsInProtectPage;
	
	@FindBy(xpath="//table[@class='table']/tbody/tr/td[1]")
	public static List<WebElement> listOfUrlsInProtectPage;
	
	@FindBy(xpath="//table[@class='table']/tbody/tr/td[1]")
	public static List<WebElement> listOfCategoryInProtectPage;
	
	@FindBy(id="protectTabId")
	public static WebElement protectTab;	
	
	public static void assignTothisPage()
	{
		PageFactory.initElements(Driver.driver, ProtectPage.class);
	}	
	
	/**
	 * This is developed by bhanjan
	 * validateProtectPage is used to 
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateProtectPage(){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			//String attributeVal = Driver.driver.findElement(By.xpath("//*[@id='protectTabId']")).getAttribute("class");
			String attributeVal =protectTab.getAttribute("class");
			String expectedVal="active";
			Assert.assertEquals(attributeVal, expectedVal, "Protect Page validation has failed...");
			log.info("Protect Page validation has sucessfully done...");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * getTotalCriticalBlockedAttackedCountInProtectPage is used to 
	 * @return
	 * This method return type is int
	 */
	public static  int getTotalCriticalBlockedAttackedCountInProtectPage(){
		String totalCriticalCount = criticalCountInProtectPage.getText();
		int count=Integer.valueOf(totalCriticalCount);
		return count;
	}
	/**
	 * This is developed by bhanjan
	 * getTotalMediumBlockedAttackedCountInProtectPage is used to 
	 * @return
	 * This method return type is int
	 */
	public static  int getTotalMediumBlockedAttackedCountInProtectPage(){
		String totalHighCount = mediumCountInProtectPage.getText();
		int count=Integer.valueOf(totalHighCount);
		return count;
	}
	/**
	 * This is developed by bhanjan
	 * getTotalHighBlockedAttackedCountInProtectPage is used to 
	 * @return
	 * This method return type is int
	 */
	public static  int getTotalHighBlockedAttackedCountInProtectPage(){
		 String totalMediumCount = highCountInProtectPage.getText();
		 int count=Integer.valueOf(totalMediumCount);
		 return count;
	}
	
	
	/**
	 * This is developed by bhanjan
	 * validateAttacksByIPInProtectPageBeforeAttack is used to 
	 * @param ls
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateAttacksByIPInProtectPageBeforeAttack(LinkedHashSet<String> ls){
		try{
			boolean val=false;
			if(ls.size()==0){
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			CommonUtil.explicitWait(10000);
			attacksByIPTab.click();
			CommonUtil.explicitWait(10000);
			
			//List<WebElement> findElements =Driver.driver.findElements(By.xpath("//table[@class='table']/tbody/tr/td[1]"));
		    LinkedHashSet<String> lhs=new LinkedHashSet<String>();
		    
		    for(int i=0;i<listOfIPsInProtectPage.size();i++){
		    	
		    	if(!(((listOfIPsInProtectPage.get(i).getText()).equals("IP"))||((listOfIPsInProtectPage.get(i).getText()).trim().isEmpty()))){
		    		lhs.add(listOfIPsInProtectPage.get(i).getText());
		    	}
		    }
		    Assert.assertTrue(ls.size()==lhs.size()," Shows some  ip address  in protect page");
			
			}
		    log.info("Validate listOfIPAddressBeforeAttack in Protect page");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * This is developed by bhanjan
	 * validateAttacksByUrlsInProtectPageBeforeAttack is used to 
	 * @param ls
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateAttacksByUrlsInProtectPageBeforeAttack(LinkedHashSet<String> ls){
		try{
			boolean val=false;
			if(ls.size()==0){
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			CommonUtil.explicitWait(10000);
			attacksByUrltab.click();
			CommonUtil.explicitWait(10000);
			
			//List<WebElement> findElements =Driver.driver.findElements(By.xpath("//table[@class='table']/tbody/tr/td[1]"));
		    LinkedHashSet<String> lhs=new LinkedHashSet<String>();
		    
		    for(int i=0;i<listOfUrlsInProtectPage.size();i++){
		    	
		    	if(!(((listOfUrlsInProtectPage.get(i).getText()).equals("URI"))||((listOfUrlsInProtectPage.get(i).getText()).trim().isEmpty()))){
		    		lhs.add(listOfUrlsInProtectPage.get(i).getText());
		    	}
		    }
		    
			
		    Assert.assertTrue(ls.size()==lhs.size()," Shows some  URLS  in protect page");
			
			}
			
		    log.info("Validate listOfURLSBeforeAttack in Protect page");
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateAttacksByIPInProtectPage is used to 
	 * @param ls
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateAttacksByIPInProtectPage(LinkedHashSet<String> ls){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			attacksByIPTab.click();
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			
		    Iterator<String> it=ls.iterator();
		   
		    
			
			while(it.hasNext()){
				String searchItem=it.next();
				log.info("Enter this value in the searchBox "+searchItem);
				searchBox.clear();
				searchBox.sendKeys(searchItem,Keys.ENTER);
				CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
				
				//List<WebElement> findElements =Driver.driver.findElements(By.xpath("//table[@class='table']/tbody/tr/td[1]"));
			    LinkedHashSet<String> lhs=new LinkedHashSet<String>();
				
			    for(int i=0;i<listOfIPsInProtectPage.size();i++){
			    	
			    	if(!((listOfIPsInProtectPage.get(i).getText()).equals("IP"))){
			    		lhs.add(listOfIPsInProtectPage.get(i).getText());
			    	}
			    }
				
				Assert.assertTrue(lhs.contains(searchItem),searchItem+" this  ip address is not display in protect page");
				log.info("Entered this value in the searchBox "+searchItem );
				log.info(searchItem+" this ip address has validate successfully in protect page...");
				CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			}
				
			
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateAttacksByUrlsInProtectPage is used to 
	 * @param ls
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateAttacksByUrlsInProtectPage(LinkedHashSet<String> ls){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			attacksByUrltab.click();
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			 Iterator<String> it=ls.iterator();
		    
		    while(it.hasNext()){
				String searchItem=it.next();
				
				log.info("Enter this value in the searchBox "+searchItem);
				searchBox.clear();
				searchBox.sendKeys(searchItem,Keys.ENTER);
				CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
				//List<WebElement> findElements =Driver.driver.findElements(By.xpath("//table[@class='table']/tbody/tr/td[1]"));
			    LinkedHashSet<String> lhs=new LinkedHashSet<String>();
			   
			   
			    for(int i=0;i<listOfUrlsInProtectPage.size();i++){
			    	
			    	if(!((listOfUrlsInProtectPage.get(i).getText()).equals("URI"))){
			    		lhs.add(listOfUrlsInProtectPage.get(i).getText());
			    	}
			    }
				log.info("Entered this value in the searchBox "+searchItem );
				Assert.assertTrue(lhs.contains(searchItem),searchItem+" this  Url is not display in protect page");
				log.info(searchItem+" this URL address has validate successfully in protect page...");
				CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by bhanjan
	 * validateAttacksByCategoryInProtectPage is used to 
	 * @param ls
	 * @return
	 * This method return type is boolean
	 */
	public static boolean validateAttacksByCategoryInProtectPage(LinkedHashSet<String> ls){
		try{
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			CommonUtil.explicitWait(1000);
			attacksByCategoryTab.click();
			CommonUtil.implicitWait(Constants.IMPLICIT_WAIT_LOW);
			 Iterator<String> it=ls.iterator();
		    
		    while(it.hasNext()){
				String searchItem=it.next();
				
				log.info("Enter this value in the searchBox "+searchItem);
				searchBox.clear();
				searchBox.sendKeys(searchItem,Keys.ENTER);
				CommonUtil.explicitWait(10000);
				//List<WebElement> findElements =Driver.driver.findElements(By.xpath("//table[@class='table']/tbody/tr/td[1]"));
			    LinkedHashSet<String> lhs=new LinkedHashSet<String>();
			   
			   
			    for(int i=0;i<listOfCategoryInProtectPage.size();i++){
			    	
			    	if(!((listOfCategoryInProtectPage.get(i).getText()).equals("Category"))){
			    		lhs.add(listOfCategoryInProtectPage.get(i).getText());
			    	}
			    }
				log.info("Entered this value in the searchBox "+searchItem );
				Assert.assertTrue(lhs.contains(searchItem),searchItem+" this  Category is not display in protect page");
				log.info(searchItem+" this Category has validate successfully in protect page...");
				CommonUtil.explicitWait(1000);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * This is developed by nagendra
	 * getActiveTab is used to get the active tab name
	 * @return
	 * This method return type is String
	 */
	public static String getActiveTab(){
		String activeTabText =  activeTab.getText();
		return activeTabText;
	}	
	
	/**
	 * This is developed by nagendra
	 * validateAttacksByIpTableWithoutData is used to Validate Attacks by ip table is displaying or not
	 * @return
	 * This method return type is Boolean
	 */
	public static Boolean validateAttacksByIpTableWithoutData(){
		Boolean value;
		try{
		value =  attacksByIpTableWithoutData.isDisplayed();
		}catch(Exception e)
		{
			return false;
		}
		return value;
	}
	
	/**
	 * This is developed by nagendra
	 * clickOnAttacksByIPTab is used to click on AttackByIp Tab
	 * This method return type is void
	 */
	public static void clickOnAttacksByIPTab(){
		CommonUtil.clickOnLink(attacksByIPTab);
		log.info("Clicked on Attacks By Ip Tab");
	}
	
	/**
	 * This is developed by nagendra
	 * searchForIpInAttackByIp is used to clear the contents and enter searchItem into the Search box  of the Attacks by Ip tab
	 * @param searchItem
	 * This method return type is void
	 */
	public static void searchForIpInAttackByIp(String searchItem){
	searchBox.clear();
	searchBox.sendKeys(searchItem,Keys.ENTER);
	log.info(searchItem+" is entered in Search box of Attacks by Ip tab and Enter key is pressed");
	CommonUtil.explicitWait(10000);
	
	}
	
	/**
	 * This is developed by nagendra
	 * validateIpInAttackByIpTable is used to  validate whether the Ip is displayed in AttackByIp table is displayed or not
	 * @param ipAddressExpected
	 * @return
	 * This method return type is Boolean
	 */
	public static Boolean validateIpInAttackByIpTable(String ipAddressExpected){
		Boolean isTwoStringsEqual;
		String ipAddress= null;
		try{
			ipAddress = firstRowofAttacksTableContents.getText();
	}catch(Exception e)
	{
		log.info("Ip is not displayed in Attacks By Ip Table");
		return false;
	}
		isTwoStringsEqual = CommonUtil.compareTwoString(ipAddress, ipAddressExpected);
		return isTwoStringsEqual;
		}


	/**
	 * This is developed by nagendra
	 * clickOnAttacksByCategoryTab is used to  click on AttackByCategory Tab
	 * This method return type is void
	 */
	public static void clickOnAttacksByCategoryTab() {
		
		CommonUtil.clickOnLink(attacksByCategoryTab);
		log.info("Clicked on Attacks By Category Tab");
		
	}


	/**
	 * This is developed by nagendra
	 * searchForCategoryInAttackByCategory is used to clear the contents and enter searchItem into the Search box  of the Attacks by Category tab
	 * @param searchItem
	 * This method return type is void
	 */
	public static void searchForCategoryInAttackByCategory(String searchItem) {

		searchBox.clear();
		searchBox.sendKeys(searchItem,Keys.ENTER);
		log.info(searchItem+" is entered in Search box of Attacks by Category tab and Enter key is pressed");
		CommonUtil.explicitWait(10000);
		
	}


	/**
	 * This is developed by nagendra
	 * validateCategoryInAttackByCategoryTable is used to  validate whether the category is displayed in AttackByCategory table is displayed or not
	 * @param categoryExpected
	 * @return
	 * This method return type is Boolean
	 */
	public static Boolean validateCategoryInAttackByCategoryTable(String categoryExpected) {

		Boolean isTwoStringsEqual;
		String category= null;
		try{
			category = firstRowofAttacksTableContents.getText();
	}catch(Exception e)
	{
		log.info("category is not displayed in Attacks By Category Table");
		return false;
	}
		isTwoStringsEqual = CommonUtil.compareTwoString(category, categoryExpected);
		return isTwoStringsEqual;
	}
	
/**
 * This is developed by nagendra
 * clickOnAttacksByURLTab is used to  click on AttackByURL Tab
 * This method return type is void
 */
public static void clickOnAttacksByURLTab() {
		
		CommonUtil.clickOnLink(attacksByUrltab);
		log.info("Clicked on Attacks By URL Tab");
		
	}


	/**
	 * This is developed by nagendra
	 * searchForURLInAttackByURL is used to clear the contents and enter searchItem into the Search box  of the Attacks by URL tab
	 * @param searchItem
	 * This method return type is void
	 */
	public static void searchForURLInAttackByURL(String searchItem) {

		searchBox.clear();
		searchBox.sendKeys(searchItem,Keys.ENTER);
		log.info(searchItem+" is entered in Search box of Attacks by URL tab and Enter key is pressed");
		CommonUtil.explicitWait(10000);
		
	}


	/**
	 * This is developed by nagendra
	 * validateURLInAttackByURLTable is used to validate whether the URL is displayed in AttackByURL table is displayed or not
	 * @param uRLExpected
	 * @return
	 * This method return type is Boolean
	 */
	public static Boolean validateURLInAttackByURLTable(String uRLExpected) {

		Boolean isTwoStringsEqual;
		String uRL= null;
		try{
			uRL = firstRowofAttacksTableContents.getText();
	}catch(Exception e)
	{
		log.info("URL is not displayed in Attacks By URL Table");
		return false;
	}
		isTwoStringsEqual = CommonUtil.compareTwoString(uRL, uRLExpected);
		return isTwoStringsEqual;
	}
	
	public static Boolean validateSqlInjectionInAttackCategoriesChart() {

		Boolean isSqlInjectionDisplayed;
		try{
			isSqlInjectionDisplayed = CommonUtil.isElementDisplayed(sqlInjectionInAttackCategoriesChart);
		}catch(Exception e)
		{
			return false;
		}
		return isSqlInjectionDisplayed;
		
	}
	
	public static Boolean validateCrossSiteScriptingInAttackCategoriesChart() {

		Boolean isCrossSiteScriptingDisplayed;
		try{
			isCrossSiteScriptingDisplayed = CommonUtil.isElementDisplayed(crossSiteScriptingInAttackCategoriesChart);
		}catch(Exception e)
		{
			return false;
		}
		return isCrossSiteScriptingDisplayed;
		
	}
	
	public static Boolean validateHighInAttackSeveritiesChart() {

		Boolean isCrossSiteScriptingDisplayed;
		try{
			isCrossSiteScriptingDisplayed = CommonUtil.isElementDisplayed(highInAttackSeveritiesChart);
		}catch(Exception e)
		{
			return false;
		}
		return isCrossSiteScriptingDisplayed;
		
	}
	
	public static Boolean validateCriticalInAttackSeveritiesChart() {

		Boolean isCrossSiteScriptingDisplayed;
		try{
			isCrossSiteScriptingDisplayed = CommonUtil.isElementDisplayed(criticalInAttackSeveritiesChart);
		}catch(Exception e)
		{
			return false;
		}
		return isCrossSiteScriptingDisplayed;
		
	}
	
}
