package com.igwaf.tests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.igwaf.pages.AdminDashboardPage;
import com.igwaf.pages.DamnVulnerableWebApplicationPage;
import com.igwaf.pages.DetectPage;
import com.igwaf.pages.MonitorPage;
import com.igwaf.pages.MultiDomainPage;
import com.igwaf.pages.ProfilePage;
import com.igwaf.pages.ProtectPage;
import com.igwaf.pages.RegisterPage;
import com.igwaf.pages.SettingsPage;
import com.igwaf.pages.SummaryPage;
import com.igwaf.pages.UserLoginPage;
import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class BatTest extends BaseTest{
	

	public static Logger log= Logger.getLogger(BatTest.class);   
	
	/**
	 * This is developed by nagendra
	 * BeforeMethod is executed before test run and it is going to capture the method name which is
	 * used in screenshot method
	 * @param method
	 * This method return type is void
	 */
	@BeforeMethod
	public void nameBefore(Method method)
	{
			getCurrentMethodName= method.getName();
		    System.out.println("Test name: " + getCurrentMethodName);
	}
	
	/**
	 * This is developed by joancy
	 * navigateBackToMultiDomainPage is used to navigate to Multi domain page for next test case execution
	 * This method return type is void
	 */
	@AfterMethod(alwaysRun=true)
    public void navigateBackToMultiDomainPage(ITestResult result) throws IOException{
        log.info("______________________________________________________ ");
       if(!result.isSuccess()){
           takeScreenshot();           
           MultiDomainPage.clickOnIndusFaceLogo();  
           CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
       }       
       MultiDomainPage.clickOnIndusFaceLogo(); 
       CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    }
	
//	@Test ( priority=1 )
//    public void addHttpWebsite() throws Exception
//    {
//    	MultiDomainPage.assignTothisPage();
//     	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteButton);    	
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//    	MultiDomainPage.clickOnAddWebsiteButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteSection);  
//    	MultiDomainPage.enterAccountHolderNameInTextbox(CommonUtil.getPropertyValue("AccountHolderName"));
//    	MultiDomainPage.enterCreditCardNumberInTextbox(CommonUtil.getPropertyValue("creditCardNumber"));
//    	MultiDomainPage.enterCvvNumberInTextbox(CommonUtil.getPropertyValue("cvv"));
//    	MultiDomainPage.selectMonthFromDropdown(CommonUtil.getPropertyValue("month"));
//    	MultiDomainPage.selectYearFromDropdown(CommonUtil.getPropertyValue("year"));
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.domainNameTextbox);
//    	MultiDomainPage.enterDomainNameInTextbox(CommonUtil.getPropertyValue("domainName"));
//    	MultiDomainPage.selectDeploymentTypeFromDropdown(CommonUtil.getPropertyValue("deploymentType"));
//    	MultiDomainPage.enterforwardingIpAddressInTextbox(CommonUtil.getPropertyValue("forwardingIp"));
//    	CommonUtil.scrollDownPage();
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.leftHttp);
//    	MultiDomainPage.selctLeftHttp();
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
//    	MultiDomainPage.selctRightHttp();
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.submitButton);
//    	MultiDomainPage.clickOnSubmitButton();
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//    	MultiDomainPage.validateDomainName(CommonUtil.getPropertyValue("domainName"));
//    }
//	
//    @Test ( priority=2 )
//    public void addHttpsWebsite() throws Exception
//    {
//    	MultiDomainPage.assignTothisPage();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteButton); 
//    	MultiDomainPage.clickOnAddWebsiteButton();
//    	MultiDomainPage.validateAddWebsiteSection();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.accountHolderNameTextbox); 
//    	MultiDomainPage.enterAccountHolderNameInTextbox(CommonUtil.getPropertyValue("httpsAccountHolderName"));
//    	MultiDomainPage.enterCreditCardNumberInTextbox(CommonUtil.getPropertyValue("httpsCreditCardNumber"));
//    	MultiDomainPage.enterCvvNumberInTextbox(CommonUtil.getPropertyValue("httpsCvv"));
//    	MultiDomainPage.selectMonthFromDropdown(CommonUtil.getPropertyValue("httpsMonth"));
//    	MultiDomainPage.selectYearFromDropdown(CommonUtil.getPropertyValue("httpsYear"));
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.domainNameTextbox);
//    	MultiDomainPage.enterDomainNameInTextbox(CommonUtil.getPropertyValue("httpsDomainName"));
//    	MultiDomainPage.selectDeploymentTypeFromDropdown(CommonUtil.getPropertyValue("httpsDeploymentType"));
//    	MultiDomainPage.enterforwardingIpAddressInTextbox(CommonUtil.getPropertyValue("httpsForwardingIp"));
//    	CommonUtil.scrollDownPage();
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.leftHttp);
//    	MultiDomainPage.selctLeftHttps();
//    	MultiDomainPage.selctRightHttps();
//    	MultiDomainPage.clickOnNextButton();  	
//    	
//    	MultiDomainPage.enterHttpsPrivateKeyInTextbox(CommonUtil.getPropertyValue("httpsPrivateKey"));
//    	MultiDomainPage.enterHttpsChainCertificateInTextbox(CommonUtil.getPropertyValue("httpsChainCertificate"));
//    	MultiDomainPage.enterHttpsCertificateInTextbox(CommonUtil.getPropertyValue("httpsCertificate"));
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
//    	CommonUtil.scrollDownPage();
////    	HealthSummaryPage.enterPrivateKeyPassphraseInTextbox(CommonUtil.getPropertyValue("httpsPrivateKeyPassPhrase"));
//    	MultiDomainPage.clickOnSkipButtoon();
//    	MultiDomainPage.clickOnNextButton();
//    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.submitButton);
//    	MultiDomainPage.clickOnSubmitButton();
//    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//    	MultiDomainPage.validateDomainName(CommonUtil.getPropertyValue("domainName"));
//    }	
//    
  @Test ( priority=1 )
  public  void onboardSaasCustomer(){
  	
  	CommonUtil.generateAndUpdateDataToCongigFile();
		RegisterPage.assignTothisPage();
		RegisterPage.registerCustomer();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		UserLoginPage.assignTothisPage();
		CommonUtil.waitForVisibilityOfElement(UserLoginPage.userName);
		CommonUtil.loginToTAS();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.completeOnboarding();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.healthSummaryLabel);
  }
	
	
    @Test ( priority=2 )
    public void doRouteConfig()
	{
		MultiDomainPage.assignTothisPage();
		log.info("Executing Route configuration of new website using Route53 API test case");
    	boolean isValidate=MultiDomainPage.validateMultiDomainPage();
    	Assert.assertTrue(isValidate, "HealthSummary page has not validate...");
      	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteButton); 
      	Boolean validateRouteConfigurationStatus = MultiDomainPage.getConfigurationStatusByDomianName(CommonUtil.getPropertyValue("domainName"));
      	Boolean isRoutingCompleted=MultiDomainPage.isRoutingDone(validateRouteConfigurationStatus);
    	Assert.assertTrue(isRoutingCompleted, "Route has not done ...");
    	log.info("RouteConfig has Done ... ");
	}
    
    @Test ( priority=3 )
    public void validateConfigurationStatusAfterRoutingDone()
    {
      	MultiDomainPage.assignTothisPage();
      	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteButton); 
      	Boolean bol = MultiDomainPage.getConfigurationStatusByDomianName(CommonUtil.getPropertyValue("domainName"));
      	if(bol==true)
      		log.info("RouteConfig has Done ... ");
      	else
      		log.info("RouteConfig has not Done ... ");
    }
	
    @Test ( priority=4 )
    public void intiateScanforDomainName()
    {
      	MultiDomainPage.assignTothisPage();
      	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.addWebsiteButton); 
      	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
      	MultiDomainPage.clickOnScanNowButtonByDomanName(CommonUtil.getPropertyValue("domainName"));
      	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.continueButton);
      	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
      	MultiDomainPage.clickOnContinueButton();
      	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
      	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.scanRequestSection);
      	MultiDomainPage.clickOnScanRequestSectionCloseButton();
      	CommonUtil.refreshPage();
      	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
      	String latestScanDetails = MultiDomainPage.getLastScanDetails();
      	log.info(latestScanDetails);
    }
        
//    @Test ( priority=4 )
//    public void VerifyNonAdminUserUserDetails() throws Exception
//    {
//        MultiDomainPage.logOutFromTAS();
//        CommonUtil.loginToWAF();
//        AdminDashboardPage.assignTothisPage();
//        CommonUtil.waitForVisibilityOfElement(AdminDashboardPage.CustomersTabTable);
//        AdminDashboardPage.validateNameInCustomersTab(CommonUtil.getPropertyValue("companyName"));
//        AdminDashboardPage.navigateToUsersTab();
//        CommonUtil.waitForVisibilityOfElement(AdminDashboardPage.userNameInputbox);   
//        CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//        AdminDashboardPage.enterTextInSearchTextboxOfUsersTab(CommonUtil.getPropertyValue("emailId"));
////        CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//        CommonUtil.pressEnterThroughActions();
//        CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//        AdminDashboardPage.validateUserNameInUsersTab(CommonUtil.getPropertyValue("emailId"));
//        AdminDashboardPage.navigateToUserRolesTab();
//        CommonUtil.waitForVisibilityOfElement(AdminDashboardPage.userRolesTabUserRolesLabel);
//        CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
//        AdminDashboardPage.validateLoginInUserRoles(CommonUtil.getPropertyValue("emailId"));
//        AdminDashboardPage.navigateToWebsitesTab();
//        CommonUtil.waitForVisibilityOfElement(AdminDashboardPage.websitesTabWebsitesLabel);
//        AdminDashboardPage.validateDomainInWebsites(CommonUtil.getPropertyValue("domainName"));        
//        CommonUtil.loginToTAS();
//    }  
//    
    @Test ( priority = 5)
    public void updateSettingsPagewithLogOnly()
    {
        Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
	    Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
	    log.info("Navigated to MultiDomainPage successfully");
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
	    MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
		SummaryPage.assignTothisPage();
		isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate successfully...");
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
        CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
    	SummaryPage.clickOnSettingsLinkImage();
    	SettingsPage.assignTothisPage();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
 	    isSuccess=SettingsPage.validateSettingsPage();
	    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
	    log.info("Settings page has validate successfully...");
    	log.info("Validate the LogOnly");
    	Boolean isLogOnlyselected = SettingsPage.validatetheLogOnlyradiobuttonofWAFstatus();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertFalse(isLogOnlyselected, "LogOnly radiobutton is selected...");
    	log.info("LogOnly radiobutton is not selected ... ");
    }
       
	 @Test ( priority=6 )
		public void verifyMonitorPageWithZeroDetails(){
			Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
			Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
			log.info("Navigated to MultiDomainPage successfully");
			MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
			SummaryPage.assignTothisPage();	
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			SummaryPage.clickOnMonitorTab();
			MonitorPage.assignTothisPage();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			Boolean isActiveTabIsMonitorPage = CommonUtil.compareTwoString(MonitorPage.getActiveTab(), "Monitor");
			Assert.assertTrue(isActiveTabIsMonitorPage, "Monitor Tab is not active");
			log.info("Monitor Tab is active");
			Boolean isCustomRulesCountZero = CommonUtil.compareTwoString(MonitorPage.getCustomRulesCount() , "0");
			Assert.assertTrue(isCustomRulesCountZero, "Custom Rules count is not zero");
			log.info("Custom Rules count is Zero");		
			Boolean isTotalPOCsCountZero = CommonUtil.compareTwoString(MonitorPage.getTotalPOCsCount() , "0");
			Assert.assertTrue(isTotalPOCsCountZero, "Total POCs count is not zero");
			log.info("Total POCs count is Zero");
			Boolean isVulnerabilitiesCountZero = CommonUtil.compareTwoString(MonitorPage.getVulnerabilitiesCount() , "0");
			Assert.assertTrue(isVulnerabilitiesCountZero, "Vulnerabilities count is not zero");
			log.info("Vulnerabilities count is Zero");				
			Boolean isAttacksByIpTableDisplayedWithData = MonitorPage.validateAttacksByIpTableWithoutData();
			Assert.assertFalse(isAttacksByIpTableDisplayedWithData, "Attacks by IP Address table is displayed");
			log.info("Attacks by IP Address table is not displayed as expected");
		}
	 
	 @Test ( priority=6 )
		public void verifyDetectPageWithZeroDetails(){
			Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
			Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
			log.info("Navigated to MultiDomainPage successfully");
			MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
			SummaryPage.assignTothisPage();	
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			SummaryPage.clickOnDetectTab();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			DetectPage.assignTothisPage();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			Boolean isActiveTabIsMonitorPage = CommonUtil.compareTwoString(DetectPage.getActiveTab(), "Detect");
			Assert.assertTrue(isActiveTabIsMonitorPage, "Detect Tab is not active");
			log.info("Detect Tab is active");
			Boolean isCriticalCountZero = CommonUtil.compareTwoString(DetectPage.getCriticalCount() , "0");
			Assert.assertTrue(isCriticalCountZero, "Critical attacks count is not zero");
			log.info("Critical attacks count is Zero");		
			Boolean isHighCountZero = CommonUtil.compareTwoString(DetectPage.getHighCount() , "0");
			Assert.assertTrue(isHighCountZero, "High attacks count is not zero");
			log.info("High attacks count is Zero");
			Boolean isMediumCountZero = CommonUtil.compareTwoString(DetectPage.getMediumCount() , "0");
			Assert.assertTrue(isMediumCountZero, "Medium attacks count is not zero");
			log.info("Medium attacks count is Zero");		
			Boolean isScanInProgress=DetectPage.isScanInProgress();
		    Assert.assertTrue(isScanInProgress, "Scan Eigther completed or Not Started");
			log.info("Scan is in Progress label is displayed");			
		}
	 @Test ( priority=6 )
	    public void verifySummaryPageBeforeAttack()
	        {
	         log.info("Executing verifySummaryPageBeforeAttack test case... ");
	      Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
	   Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
	   log.info("Navigated to MultiDomainPage successfully");
	   
	   MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
	   SummaryPage.assignTothisPage();
	   isSuccess=SummaryPage.validateSummaryPage();
	   Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
	   log.info("Summary page has validate sucessfully...");
	   log.info("Validating Block Status count in Summary Page Before any attack...");
	   
	   Assert.assertEquals(SummaryPage.getTotalVulnerabilitiesDetectedCount(),CommonUtil.getPropertyValue("TotalVulnerabilitiesDetectedCountInSummaryPageBeforeAttack").toString());
	   log.info("Validate TotalVulnerabilitiesDetectedCount in Summary Page before any attack...");
	   
	   Assert.assertEquals(SummaryPage.getTotalBlockedAgainstDetectedVulnerabilitiesCount(),CommonUtil.getPropertyValue("TotalBlockedAgainstDetectedVulnerabilitiesCountInSummaryPageBeforeAttack").toString());
	   log.info("Validate TotalBlockedAgainstDetectedVulnerabilitiesCount in Summary Page before any attack...");
	   
	   Assert.assertEquals(SummaryPage.getTotalBlockedAttacksCount(),CommonUtil.getPropertyValue("TotalBlockedAttacksCountInSummaryPageBeforeAttack").toString());
	   log.info("Validate TotalBlockedAttacksCountBeforeAttack in Summary Page before any attack...");
	   
	   Assert.assertEquals(SummaryPage.getTotalDDoSAttacksBlockedCount(),CommonUtil.getPropertyValue("TotalDDoSAttacksBlockedCountInSummaryPageBeforeAttack").toString());
	   log.info("Validate TotalDDoSAttacksBlockedCountBeforeAttack in Summary Page before any attack...");
	   
	   
	   Assert.assertTrue(SummaryPage.validateTopFiveAttacksByIPsTableDataBeforeAttack(),"Validation of TopFiveAttacksByIPsTableDataBeforeAttack has failed...");
	   log.info("Validate TopFiveAttacksByIPsTableDataBeforeAttack in Summary Page before any attack...");
	   
	   Assert.assertTrue(SummaryPage.validateTopFiveAttacksByCountiresTableDataBeforeAttack(),"validation of TopFiveAttacksByCountiresTableDataBeforeAttack has failed...");
	   log.info("Validate TopFiveAttacksByCountiresTableDataBeforeAttack in Summary Page before any attack...");
	   
	   Assert.assertTrue(SummaryPage.validateTopFiveAttacksByUrlsTableDataBeforeAttack(),"validation of TopFiveAttacksByUrlsTableDataBeforeAttack has failed...");
	   log.info("Validate TopFiveAttacksByUrlsTableDataBeforeAttack in Summary Page before any attack...");
	   log.info("Executed of verifySummaryPageBeforeAttack test case has Passed ... ");
	  }
	 @Test ( priority = 7)
	    public void updateSettingsPagewithLogandBlock()
	    {
			Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
			Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
			log.info("Navigated to MultiDomainPage successfully");
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
			MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
		    SummaryPage.assignTothisPage();
		    isSuccess=SummaryPage.validateSummaryPage();
			Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
			log.info("Summary page has validate successfully...");
		    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		    CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
		    SummaryPage.clickOnSettingsLinkImage();
		    SettingsPage.assignTothisPage();
		    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		 	isSuccess=SettingsPage.validateSettingsPage();
		    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
			log.info("Settings page has validate successfully...");
			CommonUtil.waitForVisibilityOfElement(SettingsPage.logandblock);
		    SettingsPage.clickonlogandblockofWAFstatus();
		    log.info("Validate the LogandBlock");
		    Boolean isLogandBlockselected = SettingsPage.validatetheLogandBlockradiobuttonofWAFstatus();
		    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		    Assert.assertTrue(isLogandBlockselected, "LogandBlock radiobutton is not selected...");
		    log.info("LogandBlock radiobutton is selected ... ");
	    }
	 
	 @Test ( priority = 8)
		public void verifyProtectPageWithZeroDetails(){
			Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
			Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
			log.info("Navigated to MultiDomainPage successfully");
			MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
			SummaryPage.assignTothisPage();	
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			SummaryPage.clickOnProtectTab();
			ProtectPage.assignTothisPage();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
			Boolean isActiveTabIsProtectPage = CommonUtil.compareTwoString(ProtectPage.getActiveTab(), "Protect");
			Assert.assertTrue(isActiveTabIsProtectPage, "Protect Tab is not active");
			log.info("Protect Tab is active");		
			Boolean isCustomRulesCountZero = CommonUtil.compareTwoString(String.valueOf(ProtectPage.getTotalCriticalBlockedAttackedCountInProtectPage()) , "0");
			Assert.assertTrue(isCustomRulesCountZero, "Critical count is not zero");
			log.info("Critical count is Zero");		
			Boolean isTotalPOCsCountZero = CommonUtil.compareTwoString(String.valueOf(ProtectPage.getTotalMediumBlockedAttackedCountInProtectPage()) , "0");
			Assert.assertTrue(isTotalPOCsCountZero, "Medium count is not zero");
			log.info("Medium count is Zero");
			Boolean isVulnerabilitiesCountZero = CommonUtil.compareTwoString(String.valueOf(ProtectPage.getTotalHighBlockedAttackedCountInProtectPage()) , "0");
			Assert.assertTrue(isVulnerabilitiesCountZero, "High count is not zero");
			log.info("High count is Zero");				
			Boolean isAttacksByIpTableDisplayedWithData = ProtectPage.validateAttacksByIpTableWithoutData();
			Assert.assertFalse(isAttacksByIpTableDisplayedWithData, "Attacks by IP Address table is displayed");
			log.info("Attacks by IP Address table is not displayed as expected");
		}
//	 @Test ( priority=9 )
//	    public void performDifferentAttacks()
//	    {
//	    	MultiDomainPage.assignTothisPage();
//	    	MultiDomainPage.logOutFromTAS();
//	    	log.info("Logout from Testtas instance ... ");
//	    	
//	    	DamnVulnerableWebApplicationPage.assignTothisPage();
//	    	isSuccess = DamnVulnerableWebApplicationPage.doLogInToDamnVulnerableWebApplication();
//	    	Assert.assertTrue(isSuccess, "Login has failed to DamnVulnerableWebApplicationPage... ");
//	    	log.info("Login to DamnVulnerableWebApplicationPage has done sucessfully...");
//	    	
//	        isSuccess = DamnVulnerableWebApplicationPage.validateDamnVulnerableWebApplicationPage();
//	    	Assert.assertTrue(isSuccess,"DamnVulnerableWebApplication Home Page validation  has failed... ");
//	    	log.info("DamnVulnerableWebApplication Home Page validation has done sucessfully...");
//	    	
//	    	isSuccess= DamnVulnerableWebApplicationPage.doXssAttack();
//	    	Assert.assertTrue(isSuccess,"Xss attack validation has failed... ");
//	    	log.info("Xss attack has done sucessfully...");
//	    	
//	    	isSuccess= DamnVulnerableWebApplicationPage.doSqlInjectionAttack();
//	    	Assert.assertTrue(isSuccess,"SqlInjection attack validation has failed... ");
//	    	log.info("SqlInjection attack has done sucessfully...");
//	    	
//	    	isSuccess= DamnVulnerableWebApplicationPage.doLogOutFromDamnVulnerableWebApplication();
//	    	Assert.assertTrue(isSuccess,"LogOut validation has failed... ");
//	    	log.info("LogOut has done sucessfully...");
//	    	
//	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_HIGH);
//	    	CommonUtil.loginToTAS();
//	    	log.info("LogIn to Testtas has done sucessfully...");
//	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
//	    }	
	
	@Test ( priority = 9)
    public void updatetheIPsBlacklistedSettingsPage()
    {
		Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
		Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
		log.info("Navigated to MultiDomainPage successfully");
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
		MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
	    SummaryPage.assignTothisPage();
	    isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate successfully...");
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
	    SummaryPage.clickOnSettingsLinkImage();
	    SettingsPage.assignTothisPage();
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 	isSuccess=SettingsPage.validateSettingsPage();
	    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
		log.info("Settings page has validate successfully...");
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	CommonUtil.waitForVisibilityOfElement(SettingsPage.IPsBlacklisted);
    	SettingsPage.clickonPlusofIPsBlacklisted();
    	SettingsPage.enterIPsBlacklistintextbox(CommonUtil.getPropertyValue("ipAddress"));
    	SettingsPage.saveIPsBlacklisted();
    	log.info("Validate the IPsBlacklisted");
    	Boolean isIPsBlacklisted= SettingsPage.validatetheIPsBlacklisted(CommonUtil.getPropertyValue("ipAddress"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isIPsBlacklisted, "IPsBlacklisted is not saved...");
    	log.info("IPsBlacklisted is saved ... ");
        SettingsPage.removetheIPsBlacklisted(CommonUtil.getPropertyValue("ipAddress"));
    	log.info("Validate the removed IP from IpsBlacklisted");
    	Boolean isremoveIPsBlacklisted= SettingsPage.validateremovedIPsBlacklisted(CommonUtil.getPropertyValue("ipAddress"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isremoveIPsBlacklisted, "IPBlacklisted is not removed...");
    	log.info("IPBlacklisted is removed ... ");
     }

	
	@Test ( priority = 9 )
    public void updatetheIPsWhitelistedSettingsPage()
    {
		Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
		Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
		log.info("Navigated to MultiDomainPage successfully");
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
		MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
	    SummaryPage.assignTothisPage();
	    isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate successfully...");
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
	    SummaryPage.clickOnSettingsLinkImage();
	    SettingsPage.assignTothisPage();
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 	isSuccess=SettingsPage.validateSettingsPage();
	    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
		log.info("Settings page has validate successfully...");
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	CommonUtil.waitForVisibilityOfElement(SettingsPage.IPsWhitelisted);
    	SettingsPage.clickonPlusofIPsWhitelisted();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.enterIPsWhitelistintextbox(CommonUtil.getPropertyValue("ipAddress"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.saveIPsWhitelisted();
    	log.info("Validate the IPsWhitelisted");
    	Boolean isIPsWhitelisted= SettingsPage.validatetheIPsWhitelisted(CommonUtil.getPropertyValue("ipAddress"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isIPsWhitelisted, "IPsWhitelisted is not saved...");
    	log.info("IPsWhitelisted is saved ... ");
        SettingsPage.removetheIPsWhitelisted(CommonUtil.getPropertyValue("ipAddress"));
    	log.info("Validate the removed IP from IpsWhitelisted");
    	Boolean isremoveIPsWhitelisted= SettingsPage.validateremovedIPsWhitelisted(CommonUtil.getPropertyValue("ipAddress"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isremoveIPsWhitelisted, "IPWhitelisted is not removed...");
    	log.info("IPWhitelisted is removed ... ");
     }


	
	@Test ( priority = 9 )
    public void updatetheCountriesIPsBlacklistedSettingsPage()
    {
		Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
		Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
		log.info("Navigated to MultiDomainPage successfully");
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
		MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
	    SummaryPage.assignTothisPage();
	    isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate successfully...");
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
	    SummaryPage.clickOnSettingsLinkImage();
	    SettingsPage.assignTothisPage();
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 	isSuccess=SettingsPage.validateSettingsPage();
	    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
		log.info("Settings page has validate successfully...");
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	CommonUtil.waitForVisibilityOfElement(SettingsPage.CountriesIPsBlacklisted);
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.clickonPlusofCountriesIPsBlacklisted();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.selectCountriesIPsBlacklistintextbox(CommonUtil.getPropertyValue("country"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.saveCountriesIPsBlacklisted();
    	log.info("Validate the CountriesIPsBlacklisted");
    	Boolean isCountriesIPsBlacklisted= SettingsPage.validatetheCountriesIPsBlacklisted(CommonUtil.getPropertyValue("country"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isCountriesIPsBlacklisted, "CountriesIPsBlacklisted is not saved...");
    	log.info("CountriesIPsBlacklisted is saved ... ");
        SettingsPage.removetheCountriesIPsBlacklisted(CommonUtil.getPropertyValue("country"));
    	log.info("Validate the removed IP from CountriesIPsBlacklisted");
    	Boolean isremoveCountriesIPsBlacklisted= SettingsPage.validateremovedCountriesIPsBlacklisted(CommonUtil.getPropertyValue("country"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isremoveCountriesIPsBlacklisted, "CountriesIPsBlacklisted is not removed...");
    	log.info("CountriesIPsBlacklisted is removed ... ");
     }
	
	@Test ( priority = 9 )
     public void updatetheURLsWhitelistedSettingsPage()
    {
		Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
		Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
		log.info("Navigated to MultiDomainPage successfully");
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(MultiDomainPage.webSiteLink);
		MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
	    SummaryPage.assignTothisPage();
	    isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate successfully...");
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    CommonUtil.waitForVisibilityOfElement(SummaryPage.settings);
	    SummaryPage.clickOnSettingsLinkImage();
	    SettingsPage.assignTothisPage();
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 	isSuccess=SettingsPage.validateSettingsPage();
	    Assert.assertTrue(isSuccess, "Settings Page has not validate successfully...");
		log.info("Settings page has validate successfully...");
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	CommonUtil.waitForVisibilityOfElement(SettingsPage.URLsWhitelisted);
    	SettingsPage.clickonPlusofURLsWhitelisted();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.enterURLsWhitelistintextbox(CommonUtil.getPropertyValue("urls"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	SettingsPage.saveURLsWhitelisted();
    	log.info("Validate the URLsWhitelisted");
    	Boolean isURLsWhitelisted= SettingsPage.validatetheURLsWhitelisted(CommonUtil.getPropertyValue("urls"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isURLsWhitelisted, "IPsWhitelisted is not saved...");
    	log.info("URLsWhitelisted is saved ... ");
        SettingsPage.removetheURLsWhitelisted(CommonUtil.getPropertyValue("urls"));
    	log.info("Validate the removed IP from URLsWhitelisted");
    	Boolean isremoveURLsWhitelisted= SettingsPage.validateremovedURLsWhitelisted(CommonUtil.getPropertyValue("urls"));
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	Assert.assertTrue(isremoveURLsWhitelisted, "URLsWhitelisted is not removed...");
    	log.info("URLsWhitelisted is removed ... ");
     }

    @Test( priority= 12 )
    public void updateUserProfile()
    {
     Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
	 Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
	 log.info("Navigated to MultiDomainPage successfully");
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     CommonUtil.waitForVisibilityOfElement(MultiDomainPage.profile);
     MultiDomainPage.clickOnProfileLinkImage();
     ProfilePage.assignTothisPage();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
 	 isSuccess=ProfilePage.validateProfilePage();
	 Assert.assertTrue(isSuccess, "Profile Page has not validate successfully...");
	 log.info("Profile page has validate successfully...");
     log.info("Validate the UserName ");
	 Boolean isuserName = ProfilePage.validatetheusername(CommonUtil.getPropertyValue("emailId"));
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 Assert.assertTrue(isuserName, "UserName is not correct...");
	 log.info("UserName is correct ... ");
     CommonUtil.waitForVisibilityOfElement(ProfilePage.nameTextbox);
     ProfilePage.enterNameInTextbox(CommonUtil.getPropertyValue("newName"));
     ProfilePage.enteremailIdInTextbox(CommonUtil.getPropertyValue("newEmailId"));
     ProfilePage.enterphonenumberInTextbox(CommonUtil.getPropertyValue("newphonenumber"));
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     ProfilePage.clickOnSaveButtonofEdit();
     CommonUtil.waitForVisibilityOfElement(ProfilePage.successMessageSectionTextinEdit);
     ProfilePage.successmessageofEdit();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     ProfilePage.clickOnCloseButtonOfSucessSectioninEdit();
    } 
    
    @Test( priority= 9 )
    public void updateUserProfilewithDailySummaryReport()
    {
     Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
	 Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
	 log.info("Navigated to MultiDomainPage successfully");
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     CommonUtil.waitForVisibilityOfElement(MultiDomainPage.profile);
     MultiDomainPage.clickOnProfileLinkImage();
     ProfilePage.assignTothisPage();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
 	 isSuccess=ProfilePage.validateProfilePage();
	 Assert.assertTrue(isSuccess, "Profile Page has not validate successfully...");
	 log.info("Profile page has validate successfully...");
	 CommonUtil.waitForVisibilityOfElement(ProfilePage.dailysummaryreport);
	 Boolean isDailySummaryReportselected = ProfilePage.validatetheDailySummaryReport();
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 if(!isDailySummaryReportselected)
	 {
		 ProfilePage.clickonDailySummaryReport();
	 	log.info("Clicked on Daily Summary Report check box ");
	 }	 
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	 isDailySummaryReportselected = ProfilePage.validatetheDailySummaryReport();
	 Assert.assertTrue(isDailySummaryReportselected, "DailySummaryReport checkbox is not selected...");
	 log.info("DailySummaryReport checkbox is selected ... ");
    }
   
    @Test ( priority=13 )
    public  void verifySummaryPageAfterAttack(){
     
     log.info("Executing verifySummaryPageAfterAttack test case... ");
     boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
     Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
     log.info("Navigated to MultiDomainPage successfully");
     System.out.println(Driver.driver.getCurrentUrl());
     System.out.println(Driver.driver.getTitle());
     
     MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
     SummaryPage.assignTothisPage();
     isSuccess=SummaryPage.validateSummaryPage();
     Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
     log.info("Summary page has validate sucessfully...");
     
     
     Assert.assertEquals(SummaryPage.getTotalBlockedAttacksCount(),CommonUtil.getPropertyValue("TotalBlockedAttacksCountInSummaryPageAfterAttack").toString());
     log.info("Validate TotalBlockedAttacksCountBeforeAttack in Summary Page before any attack...");
     
     
     Assert.assertTrue(SummaryPage.validateTopFiveAttacksCategoriesTableDataAfterAttack(),"Validation of TopFiveAttacksCategoriesTableDataAfterAttack has failed...");
     log.info("TopFiveAttackscategoriesTableDataAfterAttack in Summary Page after attack has validate sucessfully...");
     
     Assert.assertTrue(SummaryPage.validateTopFiveAttacksByIPsTableDataAfterAttack(),"Validation of TopFiveAttacksByIPsTableDataAfterAttack has failed...");
     log.info("TopFiveAttacksByIPsTableDataAfterAttack in Summary Page after attack has validate sucessfully...");
     
     Assert.assertTrue(SummaryPage.validateTopFiveAttacksByCountryTableDataAfterAttack(),"Validation of TopFiveAttacksByIPsTableDataAfterAttack has failed...");
     log.info("TopFiveAttacksByCountryTableDataAfterAttack in Summary Page after attack has validate sucessfully...");
     
     Assert.assertTrue(SummaryPage.validateTopFiveAttacksByUrlsTableDataAfterAttack(),"Validation of TopFiveAttacksByUrlsTableDataAfterAttack has failed...");
     log.info("TopFiveAttacksByUrlsTableDataAfterAttack in Summary Page after attack has validate sucessfully...");
     
     
     log.info("Executed of verifySummaryPageAfterAttack test case has Passed ... ");
    }
    
    
    @Test( priority= 13 )
	public  void verifyProtectPageAfterAttack(){
    	
		Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
		Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
		log.info("Navigated to MultiDomainPage successfully");
		MultiDomainPage.clickOnWebSiteLinkByDomainName(CommonUtil.getPropertyValue("domainName"));
		SummaryPage.assignTothisPage();
		isSuccess=SummaryPage.validateSummaryPage();
		Assert.assertTrue(isSuccess, "Summary Page has not validate successfully...");
		log.info("Summary page has validate sucessfully...");
		
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		// Clicking on Protect Tab from the Summary Page
		SummaryPage.clickOnProtectTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		ProtectPage.assignTothisPage();
		// Clicking on Attack By Ip tab of Protect page
		ProtectPage.clickOnAttacksByIPTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		//Validating XSS Attack count
		Boolean isCriticalCountExpected = CommonUtil.compareTwoString(String.valueOf(ProtectPage.getTotalCriticalBlockedAttackedCountInProtectPage()) , "1");
		Assert.assertTrue(isCriticalCountExpected, "Critical count is not as expected");
		log.info("Critical count is as expected");
		
		//Validating Sql injection Attack count
		Boolean isHighCountExpected = CommonUtil.compareTwoString(String.valueOf(ProtectPage.getTotalHighBlockedAttackedCountInProtectPage()) , "1");
		Assert.assertTrue(isHighCountExpected, "High count is not as expected");
		log.info("High count is as expected");
		
		//Verifying XSS attack			
		
		//Entering ip in Serach box of the Attack By Ip Text box
		
		ProtectPage.searchForIpInAttackByIp(CommonUtil.getPropertyValue("xssIp"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		// Checking whether the entered ip is displaying in Attack By Ip table or not
		Boolean isIpDisplyedInAttackByIpTable = ProtectPage.validateIpInAttackByIpTable(CommonUtil.getPropertyValue("xssIp"));
		Assert.assertTrue(isIpDisplyedInAttackByIpTable, "Ip is not Displayed in Attacked By Ip Table");
		log.info("Ip is Displayed in Attacked By Ip Table");		
		
		//Validating AttacksByCatagory Tab
		
		// Clicking on Attack By Category tab of Protect page
		ProtectPage.clickOnAttacksByCategoryTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		//Entering Category in Serach box of the Attack By Category Text box			
		ProtectPage.searchForCategoryInAttackByCategory(CommonUtil.getPropertyValue("xssCategory"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);	
		
		// Checking whether the entered Category is displaying in Attack By Category table or not
		Boolean isIpDisplyedInAttackByCategoryTable = ProtectPage.validateCategoryInAttackByCategoryTable(CommonUtil.getPropertyValue("xssCategory"));
		Assert.assertTrue(isIpDisplyedInAttackByCategoryTable, "Category is not Displayed in Attacked By Category Table");
		log.info("Category is Displayed in Attacked By Category Table");
		
		//Validating AttacksByURL Tab
		
		// Clicking on Attack By URL tab of Protect page
		ProtectPage.clickOnAttacksByURLTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		//Entering Category in Serach box of the Attack By Category Text box			
		ProtectPage.searchForURLInAttackByURL(CommonUtil.getPropertyValue("xssURL"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		// Checking whether the entered URL is displaying in Attack By URL table or not
		Boolean isURLDisplyedInAttackByURLTable = ProtectPage.validateURLInAttackByURLTable(CommonUtil.getPropertyValue("xssURL"));
		Assert.assertTrue(isURLDisplyedInAttackByURLTable, "URL is not Displayed in Attacked By URL Table");
		log.info("URL is Displayed in Attacked By URL Table");
		
		
		// Checking whether the CrossSite Scripting in Attack Categories chart is displayed or not
		Boolean isCrossSiteScriptingInAttackCategories = ProtectPage.validateCrossSiteScriptingInAttackCategoriesChart();
		Assert.assertTrue(isCrossSiteScriptingInAttackCategories, "CrossSite Scripting in Attack Categories Chart is not displyed");
		log.info("CrossSite Scripting in Attack Categories Chart is displyed");
		
		// Checking whether the Critical is displayed in Attack Severities Chart or not
		Boolean isCriticalInAttackSeverities = ProtectPage.validateCriticalInAttackSeveritiesChart();
		Assert.assertTrue(isCriticalInAttackSeverities, "Critical in Attack Severities Chart is not displyed");
		log.info("Critical in Attack Severities Chart is displyed");
		
		//Navigating to Ip Tab
		ProtectPage.clickOnAttacksByIPTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		//Verifying Sql Injection attack
		
		//Entering ip in Serach box of the Attack By Ip Text box
		
		ProtectPage.searchForIpInAttackByIp(CommonUtil.getPropertyValue("sqlIP"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		// Checking whether the entered ip is displaying in Attack By Ip table or not
		isIpDisplyedInAttackByIpTable = ProtectPage.validateIpInAttackByIpTable(CommonUtil.getPropertyValue("sqlIP"));
		Assert.assertTrue(isIpDisplyedInAttackByIpTable, "Ip is not Displayed in Attacked By Ip Table");
		log.info("Ip is Displayed in Attacked By Ip Table");		
		
		//Validating AttacksByCatagory Tab
		
		// Clicking on Attack By Category tab of Protect page
		ProtectPage.clickOnAttacksByCategoryTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
		//Entering Category in Serach box of the Attack By Category Text box			
		ProtectPage.searchForCategoryInAttackByCategory(CommonUtil.getPropertyValue("sqlInjectionCategory"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);	
		
		// Checking whether the entered Category is displaying in Attack By Category table or not
		isIpDisplyedInAttackByCategoryTable = ProtectPage.validateCategoryInAttackByCategoryTable(CommonUtil.getPropertyValue("sqlInjectionCategory"));
		Assert.assertTrue(isIpDisplyedInAttackByCategoryTable, "Category is not Displayed in Attacked By Category Table");
		log.info("Category is Displayed in Attacked By Category Table");
		
		//Validating AttacksByURL Tab
		
		// Clicking on Attack By URL tab of Protect page
		ProtectPage.clickOnAttacksByURLTab();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);	
		
		//Entering Category in Serach box of the Attack By Category Text box			
		ProtectPage.searchForURLInAttackByURL(CommonUtil.getPropertyValue("sqlInjectionURL"));
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);	
		
		// Checking whether the entered URL is displaying in Attack By URL table or not
		isURLDisplyedInAttackByURLTable = ProtectPage.validateURLInAttackByURLTable(CommonUtil.getPropertyValue("sqlInjectionURL"));
		Assert.assertTrue(isURLDisplyedInAttackByURLTable, "URL is not Displayed in Attacked By URL Table");
		log.info("URL is Displayed in Attacked By URL Table");
		
		
		// Checking whether the Sql Injection in Attack Categories chart is displayed or not
		Boolean isSqlInjectionInAttackCategories = ProtectPage.validateSqlInjectionInAttackCategoriesChart();
		Assert.assertTrue(isSqlInjectionInAttackCategories, "Sql injection in Attack CAtegories Chart is not displyed");
		log.info("Sql injection in Attack CAtegories Chart is displyed");
		
		// Checking whether the High is displayed in Attack Severities Chart or not
		Boolean isHighInAttackSeverities = ProtectPage.validateHighInAttackSeveritiesChart();
		Assert.assertTrue(isHighInAttackSeverities, "High in Attack Severities Chart is not displyed");
		log.info("High in Attack Severities Chart is displyed");
	}
    
    @Test ( priority= 12)
    public void updateUserProfilewithChangePassword()
    {
     Boolean isMultiDomainPageValidate=MultiDomainPage.validateMultiDomainPage();
	 Assert.assertTrue(isMultiDomainPageValidate, "Navigated to MultiDomainPage failed");
	 log.info("Navigated to MultiDomainPage successfully");
	 CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     CommonUtil.waitForVisibilityOfElement(MultiDomainPage.profile);
     MultiDomainPage.clickOnProfileLinkImage();
     ProfilePage.assignTothisPage();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
 	 isSuccess=ProfilePage.validateProfilePage();
	 Assert.assertTrue(isSuccess, "Profile Page has not validate successfully...");
	 log.info("Profile page has validate successfully...");
     CommonUtil.waitForVisibilityOfElement(ProfilePage.changepassword);
     ProfilePage.clickonChangePassword();
     CommonUtil.waitForVisibilityOfElement(ProfilePage.currentpassword);
     ProfilePage.enterCurrentPasswordInTextbox(CommonUtil.getPropertyValue("password"));
     ProfilePage.enterNewPasswordInTextbox(CommonUtil.getPropertyValue("password"));
     ProfilePage.enterConfirmPasswordInTextbox(CommonUtil.getPropertyValue("password"));
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     ProfilePage.clickonChangeButtonofChangePassword();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     CommonUtil.waitForVisibilityOfElement(ProfilePage.successMessageSectionTextinChangePassword);
     ProfilePage.successmessageofChangePassowrd();
     CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
     ProfilePage.clickOnCloseButtonOfSucessSectioninChangePassword();
    }
}
