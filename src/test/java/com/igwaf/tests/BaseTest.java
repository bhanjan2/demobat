package com.igwaf.tests;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.igwaf.pages.MultiDomainPage;
import com.igwaf.pages.RegisterPage;
import com.igwaf.pages.SignUpPage;
import com.igwaf.pages.SuperUserHomePage;
import com.igwaf.pages.UserLoginPage;
import com.igwaf.util.CommonUtil;
import com.igwaf.util.Constants;
import com.igwaf.util.Driver;

public class BaseTest {

	SignUpPage signUpPage=PageFactory.initElements(Driver.driver, SignUpPage.class);
	SuperUserHomePage superUserHomePage;
	public static Logger log= Logger.getLogger(BaseTest.class);
	CommonUtil util = new CommonUtil();	
	String getCurrentMethodName= null;
	public boolean isSuccess = false ;
	protected static final boolean testEnabled=false;
	
	/**
	 * This is developed by Bhanjan
	 * BeforeSuite is executed once per run and it is going to launch the browser and navigates to 
	 * specified URL and deletes Screenshots Folder
	 * @throws Exception
	 * This method return type is void
	 */
	@BeforeSuite
	public void launchBrowser() throws Exception {
		CommonUtil.launchBrowser(CommonUtil.getPropertyValue("URL"));
	}
	
	/**
	 * This is developed by nagendra
	 * BeforeClass is executed before test executes and it is going to navigate to the TAS and login
	 * functinality is called
	 * This method return type is void
	 */
	@BeforeClass
	public void loginToTAS(){
		CommonUtil.loginToTAS();
		//CommonUtil.completeOnboarding();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
    	CommonUtil.waitForVisibilityOfElement(MultiDomainPage.healthSummaryLabel);
		}
	
	/**
	 * This is developed by nagendra
	 * logoutFromTAS is used to log out from the TAS application
	 * This method return type is void
	 */
	@AfterClass
	public void logoutFromTAS(){
		MultiDomainPage.logOutFromTAS();
    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		}
	/**
	 * This is developed by nagendra
	 * AfterSuite is executed after the execution is done it is going to close the current browser by
	 * calling the method closeBrowser of CommonUtil class
	 * This method return type is void
	 */
	@AfterSuite
	public void quiteBrowser(){
//		superUserHomePage.doLogout();
		CommonUtil.closeBrowser();
		log.info("Browser Closed Successfully");	
	    CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	}
	
	/**
	 * This is developed by nagendra
	 * takeScreenshot is used to capture the screen shot and save in screenshots folder as
	 * mehtodName and date format as a jpg file
	 * @throws IOException
	 * This method return type is void
	 */
	public void takeScreenshot() throws IOException
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		Calendar cal = Calendar.getInstance();
		File imageFile = ((TakesScreenshot)Driver.driver).getScreenshotAs(OutputType.FILE);
		String screenshotimageFileName = getCurrentMethodName+"_"+dateFormat.format(cal.getTime())+".JGP";
		File screenshotImagefile = new File(System.getProperty("user.dir")+"\\Screenshots\\"+screenshotimageFileName);
		FileUtils.copyFile(imageFile , screenshotImagefile );
	}
	
}
