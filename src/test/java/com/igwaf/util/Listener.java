package com.igwaf.util;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.log4testng.Logger;

import com.igwaf.tests.BaseTest;
import java.io.IOException;

import org.testng.IClass;

public class Listener extends TestListenerAdapter{
	
	public static Logger log= Logger.getLogger(Listener.class);
	BaseTest baseTest = new BaseTest();
	@Override
	public void onTestStart(ITestResult tr) {
		log.info("Test Started....");
	}
	
	public String getMethodName(ITestResult tr) {

		return tr.getName();
	}

	@Override
	public void onTestSuccess(ITestResult tr) {

		log.info("Test '" + tr.getName() + "' PASSED");
		log.info(tr.getTestClass());

		log.info(".....");
	}

	@Override
	public void onTestFailure(ITestResult tr) {

		log.info("Test '" + tr.getName() + "' FAILED");
		log.info(".....");
		try {
			baseTest.takeScreenshot();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult tr) {
		log.info("Test '" + tr.getName() + "' SKIPPED");
		log.info(".....");
	}

	private void log(String methodName) {
		log.info(methodName);
	}

	private void log(IClass testClass) {
		log.info(testClass);
	}

}
