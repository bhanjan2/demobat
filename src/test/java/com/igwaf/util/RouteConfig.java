package com.igwaf.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class RouteConfig {
	private static final String AWS_HOST = "route53.amazonaws.com";
    private static final String SECRECT_ACCESS_KEY = "dLEb9IR2K1bCZZKi4PKDhRHsJ4BRPTXhdQe24f38";
    private static final String ACCESS_KEY_ID = "AKIAIX5K5ALKXJEGNDNQ";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private static final String HOSTED_ZONE_ID = "Z25DWV6GLPLO6H";
    
    public static final String IP_ADDRESS="54.85.235.177";
//    public static final String Name="bhanjan7.indussecure.com" ; 
//    public static final String Name="nagk13.indussecure.com" ;
//    public static final String WAF_ADDRESS="35.154.8.134";
    public static final String WAF_ADDRESS="35.154.219.71";

    /**
     * Computes RFC 2104-compliant HMAC signature.
     * * @param data
     * The data to be signed.
     *
     * @param key
     *            The signing key.
     * @return
     *         The Base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException
     *             when signature generation fails
     */
    public static String calculateRFC2104HMAC(String stringToSign, String secrectAccessKey) {

        // get an hmac_sha1 key from the raw key bytes
        SecretKeySpec signingKey =
                                   new SecretKeySpec(secrectAccessKey.getBytes(),
                                                     HMAC_SHA1_ALGORITHM);

        // get an hmac_sha1 Mac instance and initialize with the signing key
        Mac mac = null;
        try {
            mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        // compute the hmac on input data bytes
        byte[] rawHmac = mac.doFinal(stringToSign.getBytes());

        // base64-encode the hmac
        return new String(Base64.encodeBase64(rawHmac));

    }
    
    /**
     * This is developed by bhanjan
     * listResourceRecords is used to shiow list of Records in AWS 
     * This method return type is void
     */
    public static void listResourceRecords() {
        // create a post request to addAPI.
        HttpClient httpclient = new DefaultHttpClient();
        String endPoint =
                          "https://route53.amazonaws.com/2013-04-01/hostedzone/" + HOSTED_ZONE_ID +
                                  "/rrset";
        HttpGet httpGet = new HttpGet(endPoint);
        try {
            authenticateAWS(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String responseString = null;
        try {
            HttpResponse response = httpclient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(responseString);
    }

    /**
     * This is developed by bhanjan
     * createResourceRecords is used to createResourceRecords
     * @param request
     * This method return type is void
     */
    public static void createResourceRecords(String request) {
        // create a post request to addAPI.
        HttpClient httpclient = new DefaultHttpClient();
        String endPoint =
                          "https://route53.amazonaws.com/2013-04-01/hostedzone/" + HOSTED_ZONE_ID +
                                  "/rrset";
        HttpPost httpAction = new HttpPost(endPoint);
        try {
            authenticateAWS(httpAction);

            httpAction.setEntity(new StringEntity(request, "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String responseString = null;
        try {
            HttpResponse response = httpclient.execute(httpAction);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(responseString);
    }

    /**
     * This is developed by bhanjan
     * authenticateAWS is used to authenticatAWS user
     * @param httpAction
     * This method return type is void
     */
    public static void authenticateAWS(HttpRequestBase httpAction) {

        String stringToSign = getGMTTime();
        httpAction.setHeader("Content-Type", "text/xml");
        httpAction.setHeader("Host", AWS_HOST);
        httpAction.setHeader("x-amz-date", stringToSign);
        String authHeaderval =
                               "AWS3-HTTPS AWSAccessKeyId=" + ACCESS_KEY_ID + ",Algorithm=" +
                                       HMAC_SHA1_ALGORITHM + ",Signature=" +
                                       calculateRFC2104HMAC(stringToSign, SECRECT_ACCESS_KEY);
        httpAction.setHeader("X-Amzn-Authorization", authHeaderval);

    }

    /*
     * Get the current date from the Amazon Route 53 server
     */
    public static String getGMTTime() {

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("https://route53.amazonaws.com/date");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpGet);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String date = response.getFirstHeader("Date").getValue();
        return date;

    }

    /**
     * This is developed by bhanjan
     * prepareCNAMERecordsReq is used to create data in xml format
     * @param action
     * @param name
     * @param ipaddress
     * @return
     * This method return type is String
     */
    public static String prepareCNAMERecordsReq(String action, String name, String ipaddress) {
        StringBuffer request =
                               new StringBuffer(
                                                "<ChangeResourceRecordSetsRequest xmlns=\"https://route53.amazonaws.com/doc/2013-04-01/\">"
         
                                                		+ "<ChangeBatch>" + "<Changes>");
        
        
        request.append("<Change><Action>" + action + "</Action>" + "<ResourceRecordSet>" +
                "<Name>" +name+ "</Name>" + "<Type>A</Type>" +
                "<TTL>60</TTL>" + "<ResourceRecords>" + "<ResourceRecord>" +
                "<Value>" +ipaddress+ "</Value>" + "</ResourceRecord>" +
                "</ResourceRecords>" + "</ResourceRecordSet>" + "</Change>");		
      request.append("</Changes>" + "</ChangeBatch>" + "</ChangeResourceRecordSetsRequest>");
      return request.toString();
    }
    
    /**
     * This is developed by bhanjan
     * routeConfigResourceRecords is used to do routeconfig
     * @param request
     * This method return type is void
     */
    public static void routeConfigResourceRecords(String request) {
        // create a post request to addAPI.
        HttpClient httpclient = new DefaultHttpClient();
        String endPoint =
                          "https://route53.amazonaws.com/2013-04-01/hostedzone/" + HOSTED_ZONE_ID +
                                  "/rrset";
        HttpPost httpAction = new HttpPost(endPoint);
        try {
            authenticateAWS(httpAction);

            httpAction.setEntity(new StringEntity(request, "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String responseString = null;
        try {
            HttpResponse response = httpclient.execute(httpAction);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(responseString);
    }
	

} 
