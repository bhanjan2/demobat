package com.igwaf.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Driver {
public static WebDriver driver = browserFactory();
	
	public static WebDriver browserFactory(){
		String browser = CommonUtil.getPropertyValue("BROWSER_TYPE");
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("test-type");
			options.addArguments("disable-popup-blocking");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);  
			driver = new ChromeDriver(capabilities);
		}else if(browser.equalsIgnoreCase("FireFox")){
			 System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver.exe");
		       driver = new FirefoxDriver();
		}
		return driver;
		
	}
}


