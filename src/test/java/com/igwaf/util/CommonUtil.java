package com.igwaf.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.igwaf.pages.MultiDomainPage;
import com.igwaf.pages.UserLoginPage;
import com.igwaf.pages.WAFLoginPage;
import com.igwaf.tests.BaseTest;

public class CommonUtil {
	public static Properties props = new Properties();
	static InputStream input = null;
	public static PropertiesConfiguration config=null;
	public static Logger log= Logger.getLogger(CommonUtil.class);
	
	/**
	 * This is developed by Bhanjan
	 * getPropertyValue is used to get the specified property value from config.Properties
	 * @param property 
	 * @return
	 * This method return type is String
	 */
	public static String getPropertyValue(String property){
		try {
			
			input = new FileInputStream("src/test/resources/config.properties");
    		props.load(input);
    		}catch (IOException ex) {
    			ex.printStackTrace();
    			} finally {
    				if (input != null) {
    					try {
    						input.close();
    						} catch (IOException e) {
    							e.printStackTrace();
    							}
    					}
    				
    			}
		return props.getProperty(property);
		}
	
	
	/**
	 * This is developed by Bhanjan
	 * setPropertyValue is used to set property value in config.properties
	 * @param MaliciousText
	 * @param IncidentTime
	 * @param IncidentID
	 * This method return type is void
	 */
	public static void setPropertyValue(String MaliciousText,String IncidentTime,String IncidentID)  {
		try{
		config = new PropertiesConfiguration("src/test/resources/Config.properties");
		config.setProperty("MaliciousText", MaliciousText);
		config.setProperty("IncidentTime", IncidentTime);
		config.setProperty("IncidentID", IncidentID);
		config.save();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	/**
	 * This is developed by Bhanjan
	 * getTitle is used to get title of the current window
	 * @return
	 * This method return type is String
	 */
	public static String getTitle(){
		return Driver.driver.getTitle();
	}
	
	/**
	 * This is developed by Bhanjan
	 * implicitWait is used to make driver to wait for specified time when ever navigation is happening
	 * @param timetowait
	 * This method return type is void
	 */
	public static void implicitWait(int timetowait){
		Driver.driver.manage().timeouts().implicitlyWait(timetowait, TimeUnit.SECONDS);
	}
	
	/**
	 * This is developed by Bhanjan
	 * launchBrowser is used to launch the browser and navigate to the specified URL and maximize the window
	 * @param URL
	 * This method return type is void
	 */
	public static void launchBrowser(String URL){
		Driver.driver.get(URL);
		Driver.driver.manage().window().maximize();
		implicitWait(60);
		
	}
	
	/**
	 * This is developed by Bhanjan
	 * explicitWait is used to stop the execution time for specified milliseconds
	 * @param timetowait
	 * This method return type is void
	 */
	public static void explicitWait(int timetowait){
		try {
			Thread.sleep(timetowait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void waitForVisibilityOfElement(WebElement ele){
		
		WebDriverWait wait = new WebDriverWait(Driver.driver, 60);
		wait.until(ExpectedConditions.visibilityOf(ele));
	}
	
	
  public static void waitForElementToBeClickable(WebElement ele){
		
		WebDriverWait wait = new WebDriverWait(Driver.driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}
  
  
  /*public static void waitForEleoBeClickable(WebElement ele){
		
		WebDriverWait wait = new WebDriverWait(Driver.driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}*/
	/**
	 * This is developed by Bhanjan
	 * closeBrowser is used to close the current browser
	 * This method return type is void
	 */
	public static void closeBrowser(){
		Driver.driver.close();
	}
	
	/**
	 * This is developed by Bhanjan
	 * refreshPage is used to refresh or reload the page
	 * This method return type is void
	 */
	public static void refreshPage(){
		Driver.driver.navigate().refresh();
		implicitWait(50);
		
	}
	
	/**
	 * This is developed by Bhanjan
	 * navigateBack is used to Navigate to the previous page
	 * This method return type is void
	 */
	public static void navigateBack(){
		try{
			implicitWait(50);
			Driver.driver.navigate().back();
			implicitWait(50);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
		
		/**
		 * This is developed by nagendra
		 * getText is used to Get the text of the specified element
		 * @param xpath
		 * @return
		 * This method return type is String
		 */
		public static String getText(WebElement ele)
		{
			String str= ele.getText();
			return str;
		}
		
				
		/**
		 * This is developed by nagendra
		 * acceptAlert is used to switch to the alert and accepting the alert
		 * This method return type is void
		 */
		public static void acceptAlert()
		{
			Alert alt= Driver.driver.switchTo().alert();
			alt.accept();
			
		}
		
		/**
		 * This is developed by nagendra
		 * dismissAlert is used to to switch to the alert and reject the alert
		 * This method return type is void
		 */
		public static void dismissAlert()
		{
			Alert alt= Driver.driver.switchTo().alert();
			alt.dismiss();			
		}
		
		/**
		 * This is developed by nagendra
		 * getTextOfAlert is used to to switch to the alert and to get the text of the alert
		 * @return
		 * This method return type is String
		 */
		public static String getTextOfAlert()
		{
			Alert alt= Driver.driver.switchTo().alert();
			return alt.getText();			
		}
		
		/**
		 * This is developed by nagendra
		 * isElementDisplayed is used to check whether the specified element is displayed in the page or not
		 * @param ele
		 * @return
		 * This method return type is boolean
		 */
		public static boolean isElementDisplayed(WebElement ele){
			
			boolean eleBooleanValue = ele.isDisplayed();
			return eleBooleanValue;
		}
		
		/**
		 * This is developed by nagendra
		 * enterTextInTextBox is used to enter value in specified element
		 * @param ele is WebElement on which we need send the text
		 * @param value is the text that need to insert to element
		 * This method return type is void
		 */
		public static void enterTextInTextBox(WebElement ele, String value)
		{
		ele.sendKeys(value);
		log.info("entered "+ value + " in respective Textbox");
		}
		
		/**
		 * This is developed by nagendra
		 * clickCheckBox is used to select the check box and as well as un check the check box.
		 * if it is selected then if we perform this method then check box has to un check vice versa
		 * @param checkbox
		 * This method return type is void
		 */
		public static void clickCheckBox(WebElement checkbox)
		{
		checkbox.click();
		log.info("clicked on check box");
		}
		
		/**
		 * This is developed by nagendra
		 * clickOnLink is used to used to click on the link or Tabs
		 * @param link
		 * This method return type is void
		 */
		public static void clickOnLink(WebElement link)
		{
			link.click();
			log.info("clicked on Link");
		}
		
		/**
		 * This is developed by nagendra
		 * clickOnButton is used to click on Button
		 * in case of navigation to next page after clicking on it please assign next page to the current driver
		 * @param button
		 * This method return type is void
		 */
		public static void clickOnButton(WebElement button)
		{
			button.click();
			log.info("clicked on Button");
		}
		
		/**
		 * This is developed by nagendra
		 * loginToTAS is used to to Navigate to the TAS url and perfor login by getting values 
		 * from properties file and validating whether navigation is sucessful or not by 
		 * validating 
		 * This method return type is void
		 */
		public static void loginToTAS()
		{
		Driver.driver.navigate().to(CommonUtil.getPropertyValue("TASLoginUrl"));
//	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);    	
   	UserLoginPage.assignTothisPage();
			CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    	CommonUtil.enterTextInTextBox(UserLoginPage.userName, CommonUtil.getPropertyValue("emailId"));
	    	CommonUtil.enterTextInTextBox(UserLoginPage.password, CommonUtil.getPropertyValue("password"));
	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    	CommonUtil.clickOnButton(UserLoginPage.singInButton); 
	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
	    	MultiDomainPage.assignTothisPage();
//	    	boolean isHealthSummaryLabelDisplayed = CommonUtil.isElementDisplayed(MultiDomainPage.healthSummaryLabel);
//	    	Assert.assertTrue(isHealthSummaryLabelDisplayed,"health summary lable is not displayed");
//	    	log.info("health summary lable is displayed");
		}
		
		/**
		 * This is developed by nagendra
		 * navigateToTAS is used to navigate to the TAS URL
		 * This method return type is void
		 */
		public static void navigateToTAS()
		{
			Driver.driver.navigate().to(CommonUtil.getPropertyValue("TASLoginUrl"));
	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM); 
		}
				
		/**
		 * This is developed by nagendra
		 * loginToWAF is used to Navigate to the WAF URL and login by getting user name and password
		 * from config.properties file
		 * This method return type is void
		 */
		public static void loginToWAF()
		{
			Driver.driver.navigate().to(CommonUtil.getPropertyValue("WafURL"));
	    	CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);    	
	    	WAFLoginPage.assignTothisPage();
	    	CommonUtil.enterTextInTextBox(WAFLoginPage.usernameTextbox, CommonUtil.getPropertyValue("WAFUsername"));
	    	CommonUtil.enterTextInTextBox(WAFLoginPage.passwordTextbox, CommonUtil.getPropertyValue("WAFPassword"));
	    	CommonUtil.clickOnButton(WAFLoginPage.loginButton);
		}
		
		/**
		 * This is developed by nagendra
		 * pressEnterThroughActions is used to press enter key using Actions class
		 * This method return type is void
		 */
		public static void pressEnterThroughActions()
		{
			Actions act = new Actions(Driver.driver);
			act.sendKeys(Keys.ENTER).perform();
			log.info("Enter key is pressed using actions class");
		}
		
		/**
		 * This is developed by nagendra
		 * selectOptionFromDropdownByVisibleText is used to select the specified option from the
		 * required drop down by visible text
		 * @param select 
		 * @param option
		 * select is the drop down element to which we need to perform action
		 * option is the visible text of the drop down which we need to select
		 * This method return type is void
		 */
		public static void selectOptionFromDropdownByVisibleText(WebElement select, String option)
		{
			Select sel = new Select(select);
			sel.selectByVisibleText(option);
			log.info("selected opion is: "+option);
		}

		/**
		 * This is developed by nagendra
		 * scrollDownPage is used to scroll the scroll the bar vertically
		 * This method return type is void
		 */
		public static void scrollDownPage() {
			JavascriptExecutor jse = (JavascriptExecutor) Driver.driver;
		    jse.executeScript("window.scrollBy(0,250)", "");
		    log.info("Page is scrolled down");
			
		}
		
		/**
		 * This is developed by nagendra
		 * compareTwoString is used to compare two strings returns true if both are matching else returns
		 * false
		 * @param actual
		 * @param expected
		 * @return
		 * This method return type is boolean
		 */
		public static boolean compareTwoString(String actual, String expected)
		{
			if(actual.equalsIgnoreCase(expected))
				return true;
			else
	      		return false;
		}
		
		
		/**
		 * This is developed by nagendra
		 * generateEmailId is used to  generate emailid like Bat and generates data like ddMMHHmmss format and ends with @insuface.com
		 * @return
		 * This method return type is String
		 */
		public static String generateEmailId()
	{
//		String email = CommonUtil.getPropertyValue("email");
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMHHmmss");
		Calendar cal = Calendar.getInstance();
		String email= "bat"+dateFormat.format(cal.getTime())+"@induface.com";
		return email;
	}
	
		/**
		 * This is developed by nagendra
		 * generateDomainName is used to  generate Domain Name like Bat and generates data like ddMMHHmmss format
		 * @return
		 * This method return type is String
		 */
		public static String generateDomainName()
		{
//			String email = CommonUtil.getPropertyValue("email");
			SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMHHmmss");
			Calendar cal = Calendar.getInstance();
			String domain= "bat"+dateFormat.format(cal.getTime())+".indussecure.com";
			return domain;
		}	
		
	/**
	 * This is developed by nagendra
	 * generateComanyName is used to  generate CompanyName like Indus and generates data like ddMMHHmmss format
	 * @return
	 * This method return type is String
	 */
	public static String generateComanyName()
	{
//		String email = CommonUtil.getPropertyValue("email");
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMHHmmss");
		Calendar cal = Calendar.getInstance();
		String companyName= "indus"+dateFormat.format(cal.getTime());
		return companyName;
		
	}
	
	/**
	 * This is developed by nagendra
	 * generateName is used to generate Name like IndusSecure and generates data like ddMMHHmmss format
	 * @return
	 * This method return type is String
	 */
	public static String generateName()
	{
//		String email = CommonUtil.getPropertyValue("email");
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMHHmmss");
		Calendar cal = Calendar.getInstance();
		String companyName= "indussecure"+dateFormat.format(cal.getTime());
		return companyName;
		
	}
	
	/**
	 * This is developed by nagendra
	 * generatePhoneNumber is used to Generate the 10 digit Phone number 
	 * @return
	 * This method return type is String
	 */
	public static String generatePhoneNumber()
	{
		Random ran = new Random();
		int phoneNumber1 = ran.nextInt(999999999)+910000000;
		String phoneNumber= "9"+phoneNumber1;
		return phoneNumber;
	}
	
	/**
	 * This is developed by nagendra
	 * generateAWSCustomerId is used to generate AWSCustomer id
	 * @return
	 * This method return type is String
	 */
	public static String generateAWSCustomerId()
	{
		Random ran = new Random();
		int awsCustomer1 = ran.nextInt(999999999)+110000000;
		String awsCustomer= "1"+awsCustomer1;
		return awsCustomer;
	}
	
	/**
	 * This is developed by nagendra
	 * generateUpdateData is used to generate data for following fileds and updates in config.properties file
	 * emailId
	 * domainName
	 * companyName
	 * fullName
	 * phoneNo
	 * awsCustomerId
	 * This method return type is void
	 */
	public static void generateAndUpdateDataToCongigFile()
	{

		try{
			String value = generateEmailId();
			config = new PropertiesConfiguration("src/test/resources/Config.properties");
			config.setProperty("emailId", value);
			value = generateDomainName();
			config.setProperty("domainName", value);
			value = generateComanyName();
			config.setProperty("companyName", value);
			value = generateName();
			config.setProperty("fullName", value);
			value = generatePhoneNumber();
			config.setProperty("phoneNo", value);
			value = generateAWSCustomerId();
			config.setProperty("awsCustomerId", value);
			config.save();
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	/**
	 * This is developed by joancy
	 * getcheckboxvalue is used to check whether the specified element is selected in the page or not
	 * This method return type is boolean
	 */

	public static boolean getcheckboxvalue(WebElement checkboxvalue) 
	{
		return checkboxvalue.isSelected();
		
	}


	/**
	 * This is developed by nagendra
	 * completeOnboarding is used to add website after registarion is sucessful
	 * This method return type is void
	 */
	public static void completeOnboarding() {
//		MultiDomainPage.assignTothisPage(); 
		MultiDomainPage.enterAccountHolderNameInTextbox(CommonUtil.getPropertyValue("AccountHolderName"));
		MultiDomainPage.enterCreditCardNumberInTextbox(CommonUtil.getPropertyValue("creditCardNumber"));
		MultiDomainPage.enterCvvNumberInTextbox(CommonUtil.getPropertyValue("cvv"));
		MultiDomainPage.selectMonthFromDropdown(CommonUtil.getPropertyValue("month"));
		MultiDomainPage.selectYearFromDropdown(CommonUtil.getPropertyValue("year"));
		MultiDomainPage.clickOnNextButton();
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.domainNameTextbox);
		MultiDomainPage.enterDomainNameInTextbox(CommonUtil.getPropertyValue("domainName"));
		MultiDomainPage.enterforwardingIpAddressInTextbox(CommonUtil.getPropertyValue("forwardingIp"));
		CommonUtil.scrollDownPage();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		MultiDomainPage.clickOnNextButton();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.leftHttp);
		MultiDomainPage.selctLeftHttp();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		MultiDomainPage.selctRightHttp();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_LOW);
		MultiDomainPage.clickOnNextButton();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		CommonUtil.waitForVisibilityOfElement(MultiDomainPage.submitButton);
		MultiDomainPage.clickOnSubmitButton();
		CommonUtil.explicitWait(Constants.EXPLICIT_WAIT_MEDIUM);
		
	}
	
	public static boolean isAlertPresent(){ 
	    try{ 
	        Alert a = new WebDriverWait(Driver.driver, 10).until(ExpectedConditions.alertIsPresent());
	        if(a!=null){
	            System.out.println("Alert is present");
	            Driver.driver.switchTo().alert().accept();
	            return true;
	        }else{
	            throw new Throwable();
	        }
	    } 
	    catch (Throwable e) {
	        //System.err.println("Alert isn't present!!");
	        return false; 
	    } 

	} 
	/*public static boolean isAlertPresent(){
		  boolean foundAlert=false;
		  WebDriverWait wait=new WebDriverWait(Driver.driver,5);
		  try{
		   wait.until(ExpectedConditions.alertIsPresent());
		   foundAlert=true;
		  }catch(Exception e){
		   e.printStackTrace();
		   foundAlert=false;
		  }
		  
		  return foundAlert;
		 }*/
	
}